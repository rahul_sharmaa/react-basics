import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import Header from './../../components/header.jsx';
import AddEmployeeS1 from './../../components/employees/add/step1.jsx';
import AddEmployeeS2 from './../../components/employees/add/step2.jsx';
import AddEmployeeS3 from './../../components/employees/add/step3.jsx';
import AddEmployeeS4 from './../../components/employees/add/step4.jsx';
import AddEmployeeS5 from './../../components/employees/add/step5.jsx';
import AddEmployeeS6 from './../../components/employees/add/step6.jsx';
import { bindActionCreators } from 'redux';
import { Employee } from '../../reducers/employee_reducer';
let AddEmployee = React.createClass({
    componentWillReceiveProps(nextProps) {
    },
    getInitialState() {
        return {
        };
    },
    componentDidMount() {
        console.log(this.props);
    },
    render() {
        return (
            <div>
                {this.props.currentStep === 1 && <AddEmployeeS1 />}                
                {this.props.currentStep === 2 && <AddEmployeeS2 />}
                {this.props.currentStep === 3 && <AddEmployeeS3 />}
                {this.props.currentStep === 4 && <AddEmployeeS4 />}
                {this.props.currentStep === 5 && <AddEmployeeS5 />}
                {this.props.currentStep === 6 && <AddEmployeeS6 />}
            </div>
        );
    }
});
let mapStateToProps = (state) => {
    return ({ currentStep: state.Employees.currentStep });
};
let mapDispatchToProps = (dispatch) => ({
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(AddEmployee));
