import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import Header from './../../components/header.jsx';
import ListEmployees from './../../components/employees/list.jsx';
import { bindActionCreators } from 'redux';
import * as EmployeeActions from './../../actions/employee_actions';
import { Employee } from '../../reducers/employee_reducer';
let ListEmployee = React.createClass({
    getInitialState(){
    },
    componentWillReceiveProps(nextProps) {
    },
    getInitialState() {
        return {
        };
    },
    componentWillMount() {

    },
    render() {
        return (
            <div>
                <ListEmployees />
            </div>
        );
    }
});
let mapStateToProps = (state) => {
    return ({
        taxonomies: state.User.taxonomies
    });
};
let mapDispatchToProps = (dispatch) => ({
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(ListEmployee));
