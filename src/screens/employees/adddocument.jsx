import React from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router';
import { bindActionCreators } from 'redux';
import * as EmployeeActions from './../../actions/employee_actions';
import './../../sass/employee.scss';
import './../../sass/variables-s.scss';
import Sidebar from './../../components/sidebar.jsx';

let AddDocument = React.createClass({
    render() {
        return (
            <div className="wrapper">
                <div className="row header no-margin box-shadow">
                    <div className="col-lg-12 top_head">
                        <div className="col-lg-10 right_side">
                            <div className="input-group">
                                <span className="input-group-btn">
                                    <span className="searchimage icon-search"></span>
                                </span>
                                <span className="icon-switch"><input type="text" placeholder="Search by Employee ID" /></span>
                                <span className="input-group-btn">
                                    <input name="btnsearch" type="button" value="Search" className="s-btn-default searchheader" />
                                </span>
                            </div>
                        </div>
                        <div className="pro-comp">
                            <div className="user_details pointer_cursor">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td className="hello">Hello</td>
                                        </tr>
                                        <tr>
                                            <td className="user_name">Admin </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div className="logout text-center pointer_cursor">
                                LOGOUT
                                    <span className="icon-logout"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <Sidebar />
                <div className="page-content-wrapper bg-white">
                    <div className="row employee-1">
                        <div className="col-sm-2 col-md-2">
                            <div className="stepwizard">
                                <div className="stepwizard-row setup-panel">
                                    <div className="stepwizard-step active">
                                        <a href="#step-1" className="btn-circle"></a>
                                        <p>Step 1</p>
                                    </div>
                                    <div className="stepwizard-step active">
                                        <a href="#step-2" className="btn-circle"></a>
                                        <p>Step 2</p>
                                    </div>
                                    <div className="stepwizard-step">
                                        <a href="#step-3" className="btn-circle"></a>
                                        <p>Step 3</p>
                                    </div>
                                    <div className="stepwizard-step">
                                        <a href="#step-3" className="btn-circle"></a>
                                        <p>Step 4</p>
                                    </div>
                                    <div className="stepwizard-step">
                                        <a href="#step-3" className="btn-circle"></a>
                                        <p>Step 5</p>
                                    </div>
                                    <div className="stepwizard-step">
                                        <a href="#step-3" className="btn-circle"></a>
                                        <p>Step 6</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-sm-10 col-md-10 filters input-space">
                            <div className="row">
                                <div className="col-sm-6 col-md-6 heading_addemployee">
                                    <h3>DOCUMENT AND OTHER DETAILS</h3>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-3 col-md-3">
                                    <div className="mainsubheading">Image  :</div>
                                </div>
                                <div className="col-sm-2 col-md-2 upload-btn-wrapper">
                                    <button className="btn">Upload</button>
                                    <input type="file" name="flDocument1" ref="fldDoc1" />
                                </div>
                                <div className="col-sm-6 col-md-6 step5text">
                                    <span>Size limit of 75 to 100 KB per each doc.  PDF or PNG or Jpeg</span>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-3 col-md-3">
                                    <div className="mainsubheading">right / left thumb  :</div>
                                </div>
                                <div className="col-sm-2 col-md-2 upload-btn-wrapper">
                                    <button className="btn">Upload</button>
                                    <input type="file" name="flDocument1" ref="fldDoc1" />
                                </div>
                                <div className="col-sm-6 col-md-6 step5text">
                                    <span>Size limit of 75 to 100 KB per each doc.  PDF or PNG or Jpeg</span>
                                </div>
                            </div>
                            <div className="row indivmargin">
                                <div className="col-sm-4 col-md-4">
                                    <div className="mainsubheading">individual Wage Structure:</div>
                                </div>
                                <div className="col-sm-4 col-md-4">
                                    <div className="">
                                        <input type="radio" name="rbEsic" value="0" ref="fldEsicWc" value="0" /><label className="radiolabel">Yes</label>
                                        <input type="radio" name="rbEsic" value="1" ref="fldEsicWc" value="1" /><label className="radiolabel">No</label>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-10 col-md-10">
                                    <div className="headingborder">wage structure details</div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-6 col-md-6">
                                    <input type="text" name="txtEsicNumber" ref="fldEsicNo" required />
                                    <span className="floating-label">Basic Salary</span>
                                </div>
                                <div className="col-sm-6 col-md-6">
                                    <input type="text" name="txtEsicNumber" ref="fldEsicNo" required />
                                    <span className="floating-label">D. A. (Special Allowance)</span>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-10 col-md-10">
                                    <div className="grosssalary">Gross Salary :  <span className="salaryprice">12345/-</span></div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-6 col-md-6">
                                    <input type="text" name="txtEsicNumber" ref="fldEsicNo" required />
                                    <span className="floating-label">HRA</span>
                                </div>
                                <div className="col-sm-6 col-md-6">
                                    <input type="text" name="txtEsicNumber" ref="fldEsicNo" required />
                                    <span className="floating-label">Washing Allowance</span>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-6 col-md-6">
                                    <input type="text" name="txtEsicNumber" ref="fldEsicNo" required />
                                    <span className="floating-label">Conveyance</span>
                                </div>
                                <div className="col-sm-6 col-md-6">
                                    <input type="text" name="txtEsicNumber" ref="fldEsicNo" required />
                                    <span className="floating-label">Additional Salary</span>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-10 col-md-10">
                                    <div className="grosssalary">Total Gross Salary :  <span className="salaryprice">12345/-</span></div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-10 col-md-10">
                                    <div className="headingborder">Deductions</div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-6 col-md-6">
                                    <input type="text" name="txtEsicNumber" ref="fldEsicNo" required />
                                    <span className="floating-label">PF contribution</span>
                                </div>
                                <div className="col-sm-6 col-md-6">
                                    <input type="text" name="txtEsicNumber" ref="fldEsicNo" required />
                                    <span className="floating-label">ESIC</span>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-6 col-md-6">
                                    <input type="text" name="txtEsicNumber" ref="fldEsicNo" required />
                                    <span className="floating-label">MLWF</span>
                                </div>
                                <div className="col-sm-6 col-md-6">
                                    <input type="text" name="txtEsicNumber" ref="fldEsicNo" required />
                                    <span className="floating-label">Professional Tax</span>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-6 col-md-6">
                                    <input type="text" name="txtEsicNumber" ref="fldEsicNo" required />
                                    <span className="floating-label">Deductions</span>
                                </div>
                                <div className="col-sm-6 col-md-6">
                                    <input type="text" name="txtEsicNumber" ref="fldEsicNo" required />
                                    <span className="floating-label">Deduction</span>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-10 col-md-10 text-right">
                                    <input name="btnsearch" type="button" value="Add employee" className="s-btn-addemployee custom btnright" />
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-10 col-md-10">
                                    <div className="headingborder">Allowance</div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-6 col-md-6">
                                    <input type="text" name="txtEsicNumber" ref="fldEsicNo" required />
                                    <span className="floating-label">Allowance</span>
                                </div>
                                <div className="col-sm-6 col-md-6">
                                    <input type="text" name="txtEsicNumber" ref="fldEsicNo" required />
                                    <span className="floating-label">Allowance</span>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-10 col-md-10 text-right">
                                    <input name="btnsearch" type="button" value="Add employee" className="s-btn-addemployee custom btnright" />
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-10 col-md-10">
                                    <div className="grosssalary">Net Salary :  <span className="salaryprice">12345/-</span></div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-10 col-md-10">
                                    <div className="headingborder">company contribution</div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-6 col-md-6">
                                    <input type="text" name="txtEsicNumber" ref="fldEsicNo" required />
                                    <span className="floating-label">PF contribution</span>
                                </div>
                                <div className="col-sm-6 col-md-6">
                                    <input type="text" name="txtEsicNumber" ref="fldEsicNo" required />
                                    <span className="floating-label">ESIC</span>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-6 col-md-6">
                                    <input type="text" name="txtEsicNumber" ref="fldEsicNo" required />
                                    <span className="floating-label">Gratuity</span>
                                </div>
                                <div className="col-sm-6 col-md-6">
                                    <input type="text" name="txtEsicNumber" ref="fldEsicNo" required />
                                    <span className="floating-label">Ex-Gratia</span>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-6 col-md-6">
                                    <input type="text" name="txtEsicNumber" ref="fldEsicNo" required />
                                    <span className="floating-label">MLWF</span>
                                </div>
                                <div className="col-sm-6 col-md-6">
                                    <input type="text" name="txtEsicNumber" ref="fldEsicNo" required />
                                    <span className="floating-label">Documentations Charges</span>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-6 col-md-6">
                                    <input type="text" name="txtEsicNumber" ref="fldEsicNo" required />
                                    <span className="floating-label">Training Charges</span>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-10 col-md-10">
                                    <div className="grosssalary">Wage Structure :  <span className="salaryprice">12345/-</span></div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-6 col-md-6">
                                    <input type="submit" value="Previous" name="sbtnBack" className="s-btn-primary custom previous_btn " />
                                    <input type="submit" value="Next" name="sbtnNext" className="s-btn-primary custom" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return ({ currentStep: state.Employees.currentStep });
};
let mapDispatchToProps = (dispatch) => ({
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(AddDocument));

