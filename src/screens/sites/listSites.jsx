import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import Header from './../../components/header.jsx';
import ListSites from './../../components/sites/list.jsx';
import { bindActionCreators } from 'redux';
import * as EmployeeActions from './../../actions/employee_actions';
import { Employee } from '../../reducers/employee_reducer';
let ListSite = React.createClass({
    getInitialState(){
    },
    componentWillReceiveProps(nextProps) {
    },
    getInitialState() {
        return {
        };
    },
    componentWillMount() {

    },
    render() {
        return (
            <div>
                <ListSites />
            </div>
        );
    }
});
let mapStateToProps = (state) => {
    return ({
    });
};
let mapDispatchToProps = (dispatch) => ({
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(ListSite));
