import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import Header from './../components/header.jsx';
import { bindActionCreators } from 'redux';
import * as UserActions from './../actions/user_actions';
import * as EmployeeActions from './../actions/employee_actions';
import './../sass/login.scss';

let Landing = React.createClass({
    getInitialState() {
        return {
        };
    },
    componentWillMount() {
        if('message' in this.props.userDetails){
            this.props.router.push('/employees/list');
        }
    },
    componentWillReceiveProps(nextProps) {
        if ('message' in nextProps.userDetails && !('message' in this.props.userDetails)) {
            this.props.dispatch(EmployeeActions.listAll());
        }
        if(nextProps.listEmployees.listEmployees.length){
            this.props.router.push('/employees/list');
        };
    },
    loginUser() {
        this.setState({
            isCredsInvalid: false,
            credsError: '',
        });
        var email = this.refs.fldEmail.value;
        var password = this.refs.fldPassword.value;
        if (email === "" || password === "") {
            this.setState({
                isCredsInvalid: true,
                credsError: 'Invalid Email Id/Password',
            });
        } else {
            var loginData = { email: email, password: password };
            this.props.dispatch(UserActions.userLogin((loginData)));
        }
    },
    render() {
        var _this = this;
        return (
            <div className="login">
            <div className="inner-container">
                <Header />
                {this.state.isCredsInvalid && <div className="error-txt">{this.state.credsError}</div>}
                <div class="form-group">
                    <div class="col-md-4">
                        <input id="textinput" name="txtEmail" type="text" placeholder="Email ID" ref="fldEmail" className="form-control input-md"/>
                        <input id="textinput" name="txtPassword" type="password" placeholder="Password" ref="fldPassword" className="form-control input-md"/>
                        <div className="forgot-pass"><a href="javascript:void(0)" name="forgot-pass">Forgot Password?</a></div>
                        <input id="textinput" name="sbtnLogin" type="submit" value="Login" onClick={this.loginUser} className="s-btn-primary"/>
                    </div>
                    {(this.props.userDetails && 'message' in this.props.userDetails) && <div className="success-txt">{this.props.userDetails.message}</div>}
                </div>
            </div>
            </div>
        );
    }
});
let mapStateToProps = (state) => {
    return ({ userDetails: state.User.userDetails, listEmployees: state.Employees });
};
let mapDispatchToProps = (dispatch) => ({
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(Landing));
