import React from 'react';
import ReactDOM from 'react-dom';

let Loader = React.createClass({
    componentDidMount() {
        // Any changes to loader UI must be done in below code only
        ReactDOM.render((
            <div data-reactroot=''
                className='ReactModal__Overlay ReactModal__Overlay--after-open loader-wrapper' >
                <div className='ReactModal__Content ReactModal__Content--after-open loader-body'
                    tabIndex='-1' >
                    
                </div>
            </div>), document.getElementById('loader'));
    },
    componentWillUnmount() {
        ReactDOM.render((<span></span>), document.getElementById('loader'));
    },

    render() {
        // the below code should never be changed. It must rener nothing
        return (
            <span></span>

        );
    }
});

export default Loader;

