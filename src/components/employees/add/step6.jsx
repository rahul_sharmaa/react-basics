import React from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router';
import { bindActionCreators } from 'redux';
import * as EmployeeActions from './../../../actions/employee_actions';
import './../../../sass/employee.scss';
import './../../../sass/variables-s.scss';
import Sidebar from './../../../components/sidebar.jsx';
import map from 'lodash/map';
let AddEmployeeS6 = React.createClass({
    loadPrevious() {
        this.props.dispatch(EmployeeActions.employeeLoadNext(5));
    },
    componentWillReceiveProps(nextProps){
        if (nextProps.empWageData.data && nextProps.empWageData.data.status_code === 200) {
            //this.props.dispatch(EmployeeActions.listAll());
        }
        if(nextProps.listEmployees.listEmployees.length){
            window.setTimeout(() => {this.props.router.push('/')}, 1000);
            this.props.router.push('/employees/list');
        };
    },
    componentWillMount(){
        let fields = [];
        let values = [];
        if(this.props.currentEmp.wage && this.props.currentEmp.wage.values) {
            map(this.props.currentEmp.wage.values, (val, i) => {
                fields.push({id: val.term_id, value: val.value, taxonomy_id: this.props.fields[i].taxonomy_id, name: this.props.fields[i].name})
                values.push({term_id: val.term_id, value: val.value})
            })
            this.setState({
                fields: fields
            })
        } else {
            map(this.props.fields, (val, i) => {
                values.push({term_id: val.id, value: val.value})
            })    
            this.setState({
                fields: this.props.fields
            })
        }

        this.setState({
            values: values,
            slIws: (!this.props.currentEmp.wage || !this.props.currentEmp.wage.wage_structure_applicable) ? "0" : "1"
        })        
    },
    save() {
        var payload = {};
        if(this.state.slIws == "1") {
            payload = {id: this.props.currentEmp.id, data: this.state.values, wage_structure_applicable: this.state.slIws, gross_salary: 0, total_gross_salary: 0, net_salary: 0,
                wage_structure: 0}
        } else {
            payload = {id: this.props.currentEmp.id, wage_structure_applicable: this.state.slIws}
        }
        this.props.dispatch(EmployeeActions.crudEmployeeWage(payload));
    },
    setTermValue(event){
        let fields = [];
        let values = [];
        map(this.state.fields, (val, i) => {
            if(val.id === Number(event.target.id))
                val.value = event.target.value
                fields.push(val)
                values.push({term_id: val.id, value: val.value})
            })
        this.setState({
            fields: fields,
            values: values
        })
    },
    setIws(event){
        this.setState({
            slIws: event.target.value
        })
    },
    getView(id){
        var view = [];
        for(var j = 0; j < this.state.fields.length; j++) {
            if(id === this.state.fields[j].taxonomy_id) {
                if((j == 0 || (j > 0 && j % 2 == 0))) {
                    if(j != this.state.fields.length - 1) {
                        view.push(
                            <div className="row">
                                <div className="col-sm-6 col-md-6">
                                    <input type="text" id={this.state.fields[j].id} onChange={this.setTermValue} value={this.state.fields[j].value} required />
                                    {this.state.fields[j].type === 0 && '%'}
                                    <span className="floating-label">{this.state.fields[j].name}</span>
                                </div>
                                <div className="col-sm-6 col-md-6">
                                    <input type="text" id={this.state.fields[(j + 1)].id} onChange={this.setTermValue} value={this.state.fields[(j + 1)].value} required />
                                    {this.state.fields[(j + 1)].type === 0 && '%'}
                                    <span className="floating-label">{this.state.fields[(j + 1)].name}</span>
                                </div>
                            </div>
                        );
                    }
                }
            }
        }
        return view;
    },
    render() {
        return (
            <div className="wrapper">
                <div className="row header no-margin box-shadow">
                    <div className="col-lg-12 top_head">
                        <div className="col-lg-10 right_side">
                            <div className="input-group">
                                <span className="input-group-btn">
                                    <span className="searchimage icon-search"></span>
                                </span>
                                <span className="icon-switch"><input type="text" placeholder="Search by Employee ID" /></span>
                                <span className="input-group-btn">
                                    <input name="btnsearch" type="button" value="Search" className="s-btn-default searchheader" />
                                </span>
                            </div>
                        </div>
                        <div className="pro-comp">
                            <div className="user_details pointer_cursor">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td className="hello">Hello</td>
                                        </tr>
                                        <tr>
                                            <td className="user_name">Admin </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div className="logout text-center pointer_cursor">
                                LOGOUT
                                        <span className="icon-logout"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <Sidebar />
                <div className="page-content-wrapper bg-white">
                    <div className="row employee-1">
                        <div className="col-sm-2 col-md-2">
                            <div className="stepwizard">
                                <div className="stepwizard-row setup-panel">
                                    <div className="stepwizard-step active">
                                        <a href="#step-1" className="btn-circle"></a>
                                        <p>Step 1</p>
                                    </div>
                                    <div className="stepwizard-step active">
                                        <a href="#step-2" className="btn-circle"></a>
                                        <p>Step 2</p>
                                    </div>
                                    <div className="stepwizard-step active">
                                        <a href="#step-3" className="btn-circle"></a>
                                        <p>Step 3</p>
                                    </div>
                                    <div className="stepwizard-step active">
                                        <a href="#step-3" className="btn-circle"></a>
                                        <p>Step 4</p>
                                    </div>
                                    <div className="stepwizard-step active">
                                        <a href="#step-3" className="btn-circle"></a>
                                        <p>Step 5</p>
                                    </div>
                                    <div className="stepwizard-step active">
                                        <a href="#step-3" className="btn-circle"></a>
                                        <p>Step 6</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-sm-10 col-md-10 filters input-space">
                            <div className="row">
                                <div className="col-sm-6 col-md-6 heading_addemployee">
                                    <h3>DOCUMENT AND OTHER DETAILS</h3>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-3 col-md-3">
                                    <div className="mainsubheading">Image  :</div>
                                </div>
                                <div className="col-sm-2 col-md-2 upload-btn-wrapper">
                                    <button className="btn">Upload</button>
                                    <input type="file" name="flDocument1" ref="fldDoc1" />
                                </div>
                                <div className="col-sm-6 col-md-6 step5text">
                                    <span>Size limit of 75 to 100 KB per each doc.  PDF or PNG or Jpeg</span>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-3 col-md-3">
                                    <div className="mainsubheading">right / left thumb  :</div>
                                </div>
                                <div className="col-sm-2 col-md-2 upload-btn-wrapper">
                                    <button className="btn">Upload</button>
                                    <input type="file" name="flDocument1" ref="fldDoc1" />
                                </div>
                                <div className="col-sm-6 col-md-6 step5text">
                                    <span>Size limit of 75 to 100 KB per each doc.  PDF or PNG or Jpeg</span>
                                </div>
                            </div>
                            <div className="row indivmargin">
                                <div className="col-sm-4 col-md-4">
                                    <div className="mainsubheading">Individual Wage Structure:</div>
                                </div>
                                <div className="col-sm-4 col-md-4">
                                    <div className="">
                                        <input type="radio" name="rbIWS" ref="fldIWS" value="1" onClick={this.setIws} checked={this.state.slIws == "1" ? true : false} />
                                        <label className="radiolabel">Yes</label>
                                        <input type="radio" name="rbIWS" ref="fldIWS" value="0" onClick={this.setIws} checked={this.state.slIws == "0" ? true : false} />
                                        <label className="radiolabel">No</label>
                                    </div>
                                </div>
                            </div>
                            {this.state.slIws == "1" &&
                            <div className="row">
                                <div class="col-sm-10 col-md-10">
                                    <div className="headingborder">wage structure details</div>
                                </div>
                            </div>}
                            {/*
                            <div className="row">
                                <div className="col-sm-6 col-md-6">
                                    <input type="text" name="txtEsicNumber" required="" />
                                    <span className="floating-label">Basic Salary</span>
                                </div>
                                <div className="col-sm-6 col-md-6">
                                    <input type="text" name="txtEsicNumber" required="" />
                                    <span className="floating-label">D. A. (Special Allowance)</span>
                                </div>
                            </div>*/}
                            {this.state.slIws == "1" && this.props.taxonomies && map(this.props.taxonomies, (parent, i) => {
                                return (
                                    <div>
                                    <div className="row">
                                        <div className="col-sm-10 col-md-10">
                                            <div className="headingborder">{parent.name}</div>
                                        </div>
                                    </div>
                                    {this.getView(parent.id)}
                                    {parent.id === 1 && <div className="row">
                                        <div className="col-sm-10 col-md-10">
                                            <div className="grosssalary">Total Gross Salary :  <span className="salaryprice">12345/-</span></div>
                                        </div>
                                    </div>}
                                    {parent.id === 2 && <div className="row">
                                        <div className="col-sm-10 col-md-10">
                                            <div className="grosssalary">Net Salary :  <span className="salaryprice">12345/-</span></div>
                                        </div>
                                    </div>}
                                    {parent.id === 3 && <div className="row">
                                        <div className="col-sm-10 col-md-10">
                                            <div className="grosssalary">Wage Structure :  <span className="salaryprice">12345/-</span></div>
                                        </div>
                                    </div>}
                                    </div>

                                );
                            })}
                            <table>
                                <tr>
                                    <td>
                                        <input type="submit" value="Back" onClick={this.loadPrevious}
                                            name="sbtnBack"  className="s-btn-primary custom previous_btn" />
                                        <input type="submit" value="Save" onClick={this.save}
                                            name="sbtnSave" className="s-btn-primary custom" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {

    return {
        currentStep: state.Employees.currentStep,
        currentEmp: state.Employees.currEmployee,
        empWageData: state.Employees.empWageData,
        userDetails: state.User.userDetails,
        taxonomies: state.Employees.taxonomies,
        fields: state.Employees.fields,
        value: state.Employees.values,
        listEmployees: state.Employees
    };
};

let mapDispatchToProps = (dispatch) => {
    return {
        dispatch: dispatch
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(AddEmployeeS6));
