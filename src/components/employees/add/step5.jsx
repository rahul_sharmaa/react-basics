import React from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router';
import { bindActionCreators } from 'redux';
import * as EmployeeActions from './../../../actions/employee_actions';
import './../../../sass/employee.scss';
import './../../../sass/variables-s.scss';
import Sidebar from './../../../components/sidebar.jsx';
let AddEmployeeS5 = React.createClass({
    loadPrevious() {
        this.props.dispatch(EmployeeActions.employeeLoadNext(4));
    },
    loadNext() {
        var error = false;
        if (this.state.slEsicWc == "0" && this.state.esicNo === "") {
            error = true;
            this.setState({
                enError: "ESIC Number cannot be left blank."
            })
        }
        if (!error) {
            var payload = {
                id: this.props.currentEmp.id, documents: this.state.documents, uan: this.state.uan, esic_wc: this.state.slEsicWc, esic_no: this.refs.fldEsicNo.value,
                pf_no: this.state.pf
            }
            this.props.dispatch(EmployeeActions.crudEmployeeDocument(payload));
        }
    },
    componentWillReceiveProps(nextProps) {
        if(JSON.stringify(this.props.documents) !== JSON.stringify(nextProps.documents)) {
            this.setState({
                documents: nextProps.documents
            })
            if(nextProps.currentDocument === 1) {
                this.setState({
                    uploaded: 1
                })
            } else if(nextProps.currentDocument === 2) {
                this.setState({
                    uploaded1: 1
                })
            } else if(nextProps.currentDocument === 3) {
                this.setState({
                    uploaded2: 1
                })
            }
        }
        if (nextProps.empDocumentData.data && nextProps.empDocumentData.data.status_code === 200) {
            this.props.dispatch(EmployeeActions.employeeLoadNext(6));
        }
        setTimeout(() => {
            this.setState({
                uploaded: 0,
                uploaded1: 0,
                uploaded2: 0,
            })
        }, 5000)
    },
    componentWillMount() {
        this.setState({
            doc1: "",
            doc2: "",
            doc3: "",
            documents: this.props.documents,
            slEsicWc: "0",
            esicNo: "",
            uan: "",
            pf: "",
            comments: "",
            uploaded: 0,
            uploaded1: 0,
            uploaded2: 0,
        })

        if (this.props.currentEmp && this.props.currentEmp.document) {
            this.setState({
                uan: this.props.currentEmp.document.uan,
                slEsicWc: this.props.currentEmp.document.esic_wc,
                esicNo: this.props.currentEmp.document.esic_no,
                pf: this.props.currentEmp.document.pf_no,
            })
            if(this.props.currentEmp.document.documents) {
                this.setState({
                    documents: this.props.currentEmp.document.documents,
                })
            }
        }
    },
    setEsicWc(event) {
        this.setState({
            slEsicWc: event.target.value
        })
    },
    setEsicNo(event) {
        this.setState({
            esicNo: event.target.value.trim(),
            enError: false
        })
        if (this.state.slEsicWc == "0" && event.target.value.trim() === "") {
            this.setState({
                enError: "ESIC Number cannot be left blank."
            })
        }
    },
    setUan(event) {
        this.setState({
            uan: event.target.value.trim()
        })
    },
    setPf(event) {
        this.setState({
            pf: event.target.value.trim()
        })
    },
    setComments(event) {
        this.setState({
            comments: event.target.value.trim()
        })
    },
    handleImageChange(e) {
        e.preventDefault();
        var docId = e.target.id;
        this.props.dispatch(EmployeeActions.changeCurrentDocument(Number(docId)));
        let reader = new FileReader();
        let file = e.target.files[0];

        if (!file.name.match(/.(jpg|jpeg|png)$/i)) {
            return true;
        }

        reader.onloadend = () => {
            this.setState({
                imagePreviewUrl: reader.result
            });
            
            this.props.dispatch(EmployeeActions.uploadAsset(file, docId))
        };

        reader.readAsDataURL(file);
        this.refs.fldDoc1.value = "";
        this.refs.fldDoc2.value = "";
        this.refs.fldDoc3.value = "";
    },
    render() {
        return (
            <div className="wrapper">
                <div className="row header no-margin box-shadow">
                    <div className="col-lg-12 top_head">
                        <div className="col-lg-10 right_side">
                            <div className="input-group">
                                <span className="input-group-btn">
                                    <span className="searchimage icon-search"></span>
                                </span>
                                <span className="icon-switch"><input type="text" placeholder="Search by Employee ID" /></span>
                                <span className="input-group-btn">
                                    <input name="btnsearch" type="button" value="Search" className="s-btn-default searchheader" />
                                </span>
                            </div>
                        </div>
                        <div className="pro-comp">
                            <div className="user_details pointer_cursor">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td className="hello">Hello</td>
                                        </tr>
                                        <tr>
                                            <td className="user_name">Admin </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div className="logout text-center pointer_cursor">
                                LOGOUT
                                    <span className="icon-logout"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <Sidebar />
                <div className="page-content-wrapper bg-white">
                    <div className="row employee-1">
                        <div className="col-sm-2 col-md-2">
                            <div className="stepwizard">
                                <div className="stepwizard-row setup-panel">
                                    <div className="stepwizard-step active">
                                        <a href="#step-1" className="btn-circle"></a>
                                        <p>Step 1</p>
                                    </div>
                                    <div className="stepwizard-step active">
                                        <a href="#step-2" className="btn-circle"></a>
                                        <p>Step 2</p>
                                    </div>
                                    <div className="stepwizard-step active">
                                        <a href="#step-3" className="btn-circle"></a>
                                        <p>Step 3</p>
                                    </div>
                                    <div className="stepwizard-step active">
                                        <a href="#step-3" className="btn-circle"></a>
                                        <p>Step 4</p>
                                    </div>
                                    <div className="stepwizard-step active">
                                        <a href="#step-3" className="btn-circle"></a>
                                        <p>Step 5</p>
                                    </div>
                                    <div className="stepwizard-step">
                                        <a href="#step-3" className="btn-circle"></a>
                                        <p>Step 6</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-sm-10 col-md-10 filters input-space">
                            <div className="row">
                                <div className="col-sm-6 col-md-6 heading_addemployee">
                                    <h3>DOCUMENT AND OTHER DETAILS</h3>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-3 col-md-3">
                                    <div className="mainsubheading">DOCUMENT 1  :</div>
                                </div>
                                <div className="col-sm-2 col-md-2 upload-btn-wrapper">
                                    <button className="btn">Upload</button>
                                    <input type="file" name="flDocument1" ref="fldDoc1" accept='image/*' id="1" onChange={this.handleImageChange} />
                                    {this.state.uploaded === 1 && <span>Success</span>}
                                </div>
                                <div className="col-sm-6 col-md-6 step5text">
                                    <span>Size limit of 75 to 100 KB per each doc.  PDF or PNG or Jpeg</span>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-3 col-md-3">
                                    <div className="mainsubheading">DOCUMENT 2  :</div>
                                </div>
                                <div className="col-sm-2 col-md-2 upload-btn-wrapper">
                                    <button className="btn">Upload</button>
                                    <input type="file" name="flDocument2" ref="fldDoc2" accept='image/*' id="2" onChange={this.handleImageChange}  />
                                    {this.state.uploaded2 === 1 && <span>Success</span>}
                                </div>
                                <div className="col-sm-6 col-md-6 step5text">
                                    <span>Size limit of 75 to 100 KB per each doc.  PDF or PNG or Jpeg</span>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-3 col-md-3">
                                    <div className="mainsubheading">DOCUMENT 3  :</div>
                                </div>
                                <div className="col-sm-2 col-md-2 upload-btn-wrapper">
                                    <button className="btn">Upload</button>
                                    <input type="file" name="flDocument3" ref="fldDoc3" accept='image/*' id="3" onChange={this.handleImageChange} />
                                    {this.state.uploaded3 === 1 && <span>Success</span>}
                                </div>
                                <div className="col-sm-6 col-md-6 step5text">
                                    <span>Size limit of 75 to 100 KB per each doc.  PDF or PNG or Jpeg</span>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-6 col-md-6">
                                    <div className="">
                                        <input type="radio" name="rbEsic" value="0" ref="fldEsicWc" value="0" onClick={this.setEsicWc} checked={this.state.slEsicWc == "0" ? true : false} /><label className="radiolabel">ESIC</label>
                                        <input type="radio" name="rbEsic" value="1" ref="fldEsicWc" value="1" onClick={this.setEsicWc} checked={this.state.slEsicWc == "1" ? true : false} /><label className="radiolabel">WC</label>
                                    </div>
                                </div>
                                <div className="col-sm-6 col-md-6">
                                    <input type="text" name="txtEsicNumber" ref="fldEsicNo" onChange={this.setEsicNo} value={this.state.esicNo} required />
                                    <span className="floating-label">ESIC Number</span>
                                </div>
                                {this.state.enError &&
                                    <div className="error-txt">
                                        {this.state.enError}
                                    </div>}
                            </div>
                            <div className="row">
                                <div className="col-sm-6 col-md-6">
                                    <input type="text" name="txtUan" ref="fldUan" onChange={this.setUan} value={this.state.uan} required />
                                    <span className="floating-label">UAN</span>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-6 col-md-6">
                                    <input type="text" name="txtPfNumber" ref="fldPf" onChange={this.setPf} value={this.state.pf} required />
                                    <span className="floating-label">PF Number</span>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-6 col-md-6">
                                    <input type="text" name="txtComments" ref="fldComments" onChange={this.setComments} value={this.state.comments} required />
                                    <span className="floating-label">Comments</span>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-6 col-md-6">
                                    <input type="submit" value="Previous" onClick={this.loadPrevious}
                                        name="sbtnBack" className="s-btn-primary custom previous_btn " />
                                    <input type="submit" value="Next" onClick={this.loadNext}
                                        name="sbtnNext" className="s-btn-primary custom" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {

    return {
        currentStep: state.Employees.currentStep,
        currentEmp: state.Employees.currEmployee,
        empDocumentData: state.Employees.empDocumentData,
        userDetails: state.User.userDetails,
        documents: state.Employees.documents,
        currentDocument: state.Employees.currentDocument
    };
};

let mapDispatchToProps = (dispatch) => {
    return {
        dispatch: dispatch
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(AddEmployeeS5));

