import React from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router';
import { bindActionCreators } from 'redux';
import * as EmployeeActions from './../../../actions/employee_actions';
import './../../../sass/employee.scss';
import './../../../sass/variables-s.scss';
import Sidebar from './../../../components/sidebar.jsx';
import map from 'lodash/map';

let AddEmployeeS4 = React.createClass({
    loadPrevious() {
        this.props.dispatch(EmployeeActions.employeeLoadNext(3));
    },
    loadNext() {
        var doj = this.refs.fldDOJ.value;
        var doe = this.refs.fldDateOfExit.value;
        var employeeCode = this.refs.fldEmployeeCode.value;
        var error = false;
        if (doj.trim() == "") {
            error = true;
            this.setState({
                dojError: "Date of Joining cannot be left blank."
            })
        }
        if (this.state.slReference == "0" && employeeCode.trim() == "") {
            error = true;
            this.setState({
                ecError: "Employee Code cannot be left blank."
            })
        }
        if (!error) {
            var payload = {};
            if (doe.trim()) {
                payload = {
                    id: this.props.currentEmp.id, working_status: this.state.slWorkingStatus, designation_id: this.state.slDesignation, date_of_joining: this.state.doj,
                    pcc: this.state.slPcc, bvc: this.state.slBvc, physical_status: this.state.slPhysicalStatus, referred_by: this.state.slReference,
                    referral_detail: employeeCode, date_of_exit: this.state.doe, reason_for_leaving: this.state.reasonForLeaving
                };
            } else {
                payload = {
                    id: this.props.currentEmp.id, working_status: this.state.slWorkingStatus, designation_id: this.state.slDesignation, date_of_joining: this.state.doj,
                    pcc: this.state.slPcc, bvc: this.state.slBvc, physical_status: this.state.slPhysicalStatus, referred_by: this.state.slReference,
                    referral_detail: employeeCode, reason_for_leaving: this.state.reasonForLeaving
                };
            }
            this.props.dispatch(EmployeeActions.crudEmployeeWork(payload));
        }
    },
    componentWillReceiveProps(nextProps) {
        if (nextProps.empWorkData.data && nextProps.empWorkData.data.status_code === 200) {
            this.props.dispatch(EmployeeActions.employeeLoadNext(5));
        }
    },
    componentWillMount() {
        this.setState({
            doj: "2010-10-10",
            slDesignation: "1",
            slWorkingStatus: "1",
            slPcc: "0",
            slBvc: "0",
            slPhysicalStatus: "0",
            uploadCertificate: "",
            slReference: "0",
            employeeCode: "",
            doe: "2017-10-01",
            reasonForLeaving: "",
        })

        if (this.props.currentEmp && this.props.currentEmp.work) {
            this.setState({
                doj: this.props.currentEmp.work.date_of_joining,
                slDesignation: this.props.currentEmp.work.designation_id,
                slWorkingStatus: this.props.currentEmp.work.working_status,
                slPcc: this.props.currentEmp.work.pcc,
                slBvc: this.props.currentEmp.work.bvc,
                slPhysicalStatus: this.props.currentEmp.work.physical_status,
                slReference: this.props.currentEmp.work.referred_by,
                employeeCode: this.props.currentEmp.work.referral_detail,
                doe: this.props.currentEmp.work.date_of_exit,
                reasonForLeaving: this.props.currentEmp.work.reason_for_leaving,
            });
        }
    },
    setDoj(event) {
        this.setState({
            doj: event.target.value
        })
        if (event.target.value.trim() != "") {
            this.state.dojError = false
        } else {
            this.state.dojError = "Date of Joining cannot be left blank."
        }
    },
    setDesignation(event) {
        this.setState({
            slDesignation: event.target.value
        })
        if (event.target.value.trim() != "") {
            this.state.designationError = false
        } else {
            this.state.designationError = "Designation cannot be left blank."
        }
    },
    setWorkingStatus(event) {
        this.setState({
            slWorkingStatus: event.target.value
        })
    },
    setPCC(event) {
        this.setState({
            slPcc: event.target.value
        })
    },
    setBVC(event) {
        this.setState({
            slBvc: event.target.value
        })
    },
    setPhysicalStatus(event) {
        this.setState({
            slPhysicalStatus: event.target.value
        })
    },
    setReference(event) {
        this.setState({
            slReference: event.target.value
        })
    },
    setDOE(event) {
        this.setState({
            doe: event.target.value
        })
        if (event.target.value.trim() != "") {
            this.state.doeError = false
        } else {
            this.state.doeError = "Date of Exit cannot be left blank."
        }
    },
    setEmployeeCode(event) {
        this.setState({
            employeeCode: event.target.value
        })
        if (this.state.slReference == "0" && event.target.value.trim() == "") {
            this.state.ecError = "Employee Code cannot be left blank."
        } else {
            this.state.ecError = ""
        }
    },
    setReasonForLeaving(event) {
        this.setState({
            reasonForLeaving: event.target.value
        })
        if (event.target.value.trim() != "") {
            this.state.rflError = false
        } else {
            this.state.rflError = "Reason for Leaving cannot be left blank."
        }
    },
    handleImageChange(e) {
        e.preventDefault();
        var docId = e.target.id;
        this.props.dispatch(EmployeeActions.changeCurrentDocument(Number(docId)));
        let reader = new FileReader();
        let file = e.target.files[0];

        if (!file.name.match(/.(jpg|jpeg|png)$/i)) {
            return true;
        }

        reader.onloadend = () => {
            this.setState({
                imagePreviewUrl: reader.result
            });
            
            this.props.dispatch(EmployeeActions.uploadAsset(file, docId))
        };

        reader.readAsDataURL(file);
        this.refs.fldCertificate.value = "";
    },
    render() {
        return (
            <div className="wrapper">
                <div className="row header no-margin box-shadow">
                    <div className="col-lg-12 top_head">
                        <div className="col-lg-10 right_side">
                            <div className="input-group">
                                <span className="input-group-btn">
                                    <span className="searchimage icon-search"></span>
                                </span>
                                <span className="icon-switch"><input type="text" placeholder="Search by Employee ID" /></span>
                                <span className="input-group-btn">
                                    <input name="btnsearch" type="button" value="Search" className="s-btn-default searchheader" />
                                </span>
                            </div>
                        </div>
                        <div className="pro-comp">
                            <div className="user_details pointer_cursor">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td className="hello">Hello</td>
                                        </tr>
                                        <tr>
                                            <td className="user_name">Admin </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div className="logout text-center pointer_cursor">
                                LOGOUT
                                        <span className="icon-logout"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <Sidebar />
                <div className="page-content-wrapper bg-white">
                    <div className="row employee-1">
                        <div className="col-sm-2 col-md-2">
                            <div className="stepwizard">
                                <div className="stepwizard-row setup-panel">
                                    <div className="stepwizard-step active">
                                        <a href="#step-1" className="btn-circle"></a>
                                        <p>Step 1</p>
                                    </div>
                                    <div className="stepwizard-step active">
                                        <a href="#step-2" className="btn-circle"></a>
                                        <p>Step 2</p>
                                    </div>
                                    <div className="stepwizard-step active">
                                        <a href="#step-3" className="btn-circle"></a>
                                        <p>Step 3</p>
                                    </div>
                                    <div className="stepwizard-step active">
                                        <a href="#step-4" className="btn-circle"></a>
                                        <p>Step 4</p>
                                    </div>
                                    <div className="stepwizard-step">
                                        <a href="#step-5" className="btn-circle"></a>
                                        <p>Step 5</p>
                                    </div>
                                    <div className="stepwizard-step">
                                        <a href="#step-6" className="btn-circle"></a>
                                        <p>Step 6</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-sm-10 col-md-10 filters input-space">
                            <div className="row">
                                <div className="col-sm-6 col-md-6 heading_addemployee">
                                    <h3>WORK DETAILS</h3>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-6 col-md-6">
                                    <input type="text" name="txtDoj" ref="fldDOJ" value={this.state.doj} onChange={this.setDoj} required />
                                    <span className="floating-label">Date of joining</span>
                                    {this.state.dojError &&
                                        <div className="error-txt">
                                            {this.state.dojError}
                                        </div>}
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-6 col-md-6">
                                    <select name="slDesignation" ref="fldDesignation" value={this.state.slDesignation}>
                                        {this.props.designations && map(this.props.designations, (val, i) => {
                                            return (
                                                <option value={val.id}>{val.name}</option>
                                            );
                                        })}
                                    </select>
                                    {this.state.designationError &&
                                        <div className="error-txt">
                                            {this.state.designationError}
                                        </div>}
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-6 col-md-6">
                                    <span className="side_heading">Working Status</span>
                                    <span className="radiostep4margin">
                                        <input type="radio" name="rbWorkingStatus" ref="fldWorkingStatus" value="1" checked={this.state.slWorkingStatus == "1" ? true : false}
                                            onClick={this.setWorkingStatus} /> <label className="radiolabel">Active</label>
                                        <input type="radio" name="rbWorkingStatus" value="0" ref="fldWorkingStatus" value="0" checked={this.state.slWorkingStatus == "0" ? true : false}
                                            onClick={this.setWorkingStatus} /> <label className="radiolabel">Inactive</label>
                                    </span>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-6 col-md-6">
                                    <span className="side_heading">PCC</span>
                                    <span className="radiostep4margin">
                                        <input type="radio" name="rbPCC" value="0" ref="fldPCC" checked={this.state.slPcc == "0" ? true : false}
                                            onClick={this.setPCC} /><label className="radiolabel">Positive</label>
                                        <input type="radio" name="rbPCC" value="1" ref="fldPCC" checked={this.state.slPcc == "1" ? true : false}
                                            onClick={this.setPCC} /> <label className="radiolabel">Negative</label>
                                        <input type="radio" name="rbPCC" value="2" ref="fldPCC" checked={this.state.slPcc == "2" ? true : false}
                                            onClick={this.setPCC} /><label className="radiolabel"> Not Applicable</label>
                                    </span>
                                </div>
                                <div className="col-sm-6 col-md-6">
                                    <span className="side_heading">BVC</span>
                                    <span className="radiostep4margin">
                                        <input type="radio" name="rbBVC" value="0" ref="fldBVC" checked={this.state.slBvc == "0" ? true : false}
                                            onClick={this.setBVC} /> <label className="radiolabel">Positive</label>
                                        <input type="radio" name="rbBVC" value="1" ref="fldBVC" checked={this.state.slBvc == "1" ? true : false}
                                            onClick={this.setBVC} /> <label className="radiolabel">Negative</label>
                                        <input type="radio" name="rbBVC" value="2" ref="fldBVC" checked={this.state.slBvc == "2" ? true : false}
                                            onClick={this.setBVC} /><label className="radiolabel">Not Applicable</label>
                                    </span>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-6 col-md-6">
                                    <span className="side_heading">Physical Status</span>
                                    <span className="radiostep4margin">
                                        <input type="radio" name="rbPhysicalStatus" value="0" ref="fldHandicapped" checked={this.state.slPhysicalStatus == "0" ? true : false}
                                            onClick={this.setPhysicalStatus} /><label className="radiolabel">Handicapped</label>
                                        <input type="radio" name="rbPhysicalStatus" value="1" ref="fldHandicapped" checked={this.state.slPhysicalStatus == "1" ? true : false}
                                            onClick={this.setPhysicalStatus} /> <label className="radiolabel">Not Applicable</label>
                                    </span>
                                </div>
                                <div className="col-sm-2 col-md-2">
                                    <span className="side_heading1">Upload Certificate:</span>
                                </div>
                                <div className="col-sm-4 col-md-4 upload-btn-wrapper">
                                    <button className="btn">Upload</button>
                                    <input type="file" name="certificate" ref="fldCertificate" accept='image/*' id="4" onChange={this.handleImageChange} />
                                    {this.state.uploaded === 1 && <span>Success</span>}
                                </div>
                            </div>

                            
                            <div className="row">
                                <div className="col-sm-6 col-md-6">
                                    <span className="side_heading">Reference</span>
                                    <span className="radioextramargin">
                                        <input type="radio" name="rbReference" value="0" ref="fldReference" checked={this.state.slReference == "0" ? true : false}
                                            onClick={this.setReference} /> Internal
                                                            <input type="radio" name="rbReference" value="1" ref="fldReference" checked={this.state.slReference == "1" ? true : false}
                                            onClick={this.setReference} /> External
                                                        </span>
                                </div>
                                <div className="col-sm-6 col-md-6">
                                    <input type="text" name="txtEmpoloyeeCode" ref="fldEmployeeCode" onChange={this.setEmployeeCode} value={this.state.employeeCode} required />
                                    <span className="floating-label">Employee Reference Code</span>
                                    {this.state.ecError &&
                                        <div className="error-txt">
                                            {this.state.ecError}
                                        </div>}
                                </div>


                            </div>
                            <div className="row">
                                <div className="col-sm-6 col-md-6">
                                    <input type="text" name="txtDateOfExit" ref="fldDateOfExit" value={this.state.doe} onChange={this.setDOE} required />
                                    <span className="floating-label">Date of Exit</span>
                                </div>

                                {this.state.doeError &&
                                    <div className="error-txt">
                                        {this.state.doeError}
                                    </div>}
                            </div>
                            <div className="row">
                                <div className="col-sm-6 col-md-6">
                                    <span>Reason for Leaving</span>
                                    <textarea type="text" name="txtReasonForLeaving" ref="fldReasonForLeaving" value={this.state.reasonForLeaving} onChange={this.setReasonForLeaving}></textarea>
                                    {this.state.rflError &&
                                        <div className="error-txt">
                                            {this.state.rflError}
                                        </div>}
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-6 col-md-6">
                                    <input type="submit" value="Previous" onClick={this.loadPrevious}
                                        name="sbtnBack" className="s-btn-primary custom previous_btn " />
                                    <input type="submit" value="Next" onClick={this.loadNext}
                                        name="sbtnNext" className="s-btn-primary custom" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
        currentStep: state.Employees.currentStep,
        currentEmp: state.Employees.currEmployee,
        empWorkData: state.Employees.empWorkData,
        userDetails: state.User.userDetails,
        designations: state.Employees.designations,
        documents: state.Employees.documents,
        currentDocument: state.Employees.currentDocument
    };
};

let mapDispatchToProps = (dispatch) => {
    return {
        dispatch: dispatch
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(AddEmployeeS4));

