import React from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router';
import { bindActionCreators } from 'redux';
import * as EmployeeActions from './../../../actions/employee_actions';
import './../../../sass/employee.scss';
import './../../../sass/variables-s.scss';
import Sidebar from './../../../components/sidebar.jsx';
import map from 'lodash/map';

let AddEmployeeS1 = React.createClass({
    componentWillReceiveProps(nextProps) {
        if(nextProps.empMasterData && nextProps.empMasterData.status_code === 200) {
            this.props.dispatch(EmployeeActions.employeeLoadNext(2));
        }
    },
    isEmpty(value) {
        if (value.trim() === "") {
            return true;
        }
        return false;
    },
    loadNext() {
        var name = this.refs.fldName.value;
        var state = this.refs.fldState.value;
        var city = this.refs.fldCity.value;
        var streetAdrress = this.refs.fldStreetAddress.value;
        var pincode = this.refs.fldPincode.value;
        var pStreetAdrress = this.refs.fldPStreetAddress.value;
        var pState = this.refs.fldPState.value;
        var pCity = this.refs.fldPCity.value;
        var pPincode = this.refs.fldPPincode.value;

        this.setState({
            nameError: false,
            saError: false,
            cityError: false,
            pincodeError: false,
            pSaError: false,
            pCityError: false,
            pPincodeError: false,
        });

        var error = false;
        if (this.isEmpty(name)) {
            error = true;
            this.setState({
                nameError: "Employee name cannot be left blank."
            });
        }
        if (this.isEmpty(streetAdrress)) {
            error = true;
            this.setState({
                saError: "Street Address cannot be left blank."
            });
        }
        if (this.isEmpty(pincode)) {
            error = true;
            this.setState({
                pincodeError: "Pincode cannot be left blank."
            });
        }
        if (this.isEmpty(city)) {
            error = true;
            this.setState({
                cityError: "City cannot be left blank."
            });
        }
        if (this.isEmpty(pStreetAdrress)) {
            error = true;
            this.setState({
                pSaError: "Permanent Street Address cannot be left blank."
            });
        }
        if (this.isEmpty(pCity)) {
            error = true;
            this.setState({
                pCityError: "Permanent City cannot be left blank."
            });
        }
        if (this.isEmpty(pPincode)) {
            error = true;
            this.setState({
                pPincodeError: "Permanent Pincode cannot be left blank."
            });
        }

        if (!error) {
            var payload;
            if (this.state.id) {
                payload = {
                    id: this.state.id, employee_name: name, street_address: streetAdrress, state_id: state, city_id: city, pincode: pincode,
                    pstreet_address: pStreetAdrress, pstate_id: pState, pcity_id: pCity, ppincode: pPincode, is_same: this.state.isSame
                };
            } else {
                payload = {
                    employee_name: name, street_address: streetAdrress, state_id: state, city_id: city, pincode: pincode,
                    pstreet_address: pStreetAdrress, pstate_id: pState, pcity_id: pCity, ppincode: pPincode, is_same: this.state.isSame
                };
            }

            this.props.dispatch(EmployeeActions.crudEmployeeMaster(payload));
        }
    },
    componentWillMount() {

        this.setState({
            employeeName: "",
            streetAddress: "",
            slState: 1,
            slCity: 1,
            pincode: "",
            pStreetAddress: "",
            slPState: 0,
            slPCity: 0,
            pPincode: "",
            isSame: 0,
        });

        if (this.props.currentEmp && this.props.currentEmp.id) {
            this.setState({
                id: this.props.currentEmp.id,
                employeeName: this.props.currentEmp.master.employee_name,
                streetAddress: this.props.currentEmp.master.street_address,
                slState: this.props.currentEmp.master.state_id,
                slCity: this.props.currentEmp.master.city_id,
                pincode: this.props.currentEmp.master.pincode,
                pStreetAddress: this.props.currentEmp.master.pstreet_address,
                slPState: this.props.currentEmp.master.pstate_id,
                slPCity: this.props.currentEmp.master.pcity_id,
                pPincode: this.props.currentEmp.master.ppincode,
                isSame: this.props.currentEmp.master.is_same,
            });
            if( this.props.currentEmp.master.street_address === this.props.currentEmp.master.pstreet_address &&
                this.props.currentEmp.master.state_id === this.props.currentEmp.master.pstate_id &&
                this.props.currentEmp.master.city_id === this.props.currentEmp.master.pcity_id &&
                this.props.currentEmp.master.pincode === this.props.currentEmp.master.ppincode ) {
                    this.setState({
                        isSame: true
                    })
                }
        }
    },
    setEmployeeName(event) {
        this.setState({
            employeeName: event.target.value
        });
        console.log(this.state);
        if (event.target.value.trim() != "") {
            this.state.nameError = false
        } else {
            this.state.nameError = "Employee name cannot be left blank."
        }
    },
    setStreetAddress(event) {
        this.setState({
            streetAddress: event.target.value
        });
        if (event.target.value.trim() != "") {
            this.state.saError = false
        } else {
            this.state.saError = "Street Address cannot be left blank."
        }
    },
    setPincode(event) {
        this.setState({
            pincode: event.target.value
        });
        if (event.target.value.trim() != "") {
            this.state.pincodeError = false
        } else {
            this.state.pincodeError = "Pincode cannot be left blank."
        }
    },
    setPStreetAddress(event) {
        this.setState({
            pStreetAddress: event.target.value
        });
        if (event.target.value.trim() != "") {
            this.state.pSaError = false
        } else {
            this.state.pSaError = "Permanent Street Address cannot be left blank."
        }
    },
    setPPincode(event) {
        this.setState({
            pPincode: event.target.value
        });
        if (event.target.value.trim() != "") {
            this.state.pPincodeError = false
        } else {
            this.state.pPincodeError = "Permanent Pincode cannot be left blank."
        }
    },
    selectState(event) {
        this.setState({
            slState: event.target.value
        });
    },
    selectCity(event) {
        this.setState({
            slCity: event.target.value
        });
    },
    selectPState(event) {
        this.setState({
            slPState: event.target.value
        });
    },
    selectPCity(event) {
        this.setState({
            slPCity: event.target.value
        });
    },
    isSame() {

        if (this.refs.fldSP.checked) {
            this.setState({
                slPState: this.state.slState,
                slPCity: this.state.slCity,
                isSame: true,
                pStreetAddress: this.state.streetAddress,
                pPincode: this.state.pincode,
            });
        } else {
            this.setState({
                slPState: 0,
                slPCity: 0,
                isSame: false,
                pStreetAddress: "",
                pPincode: "",
            })
        }
    },
    render() {
        var _this = this;
        return (
            <div className="wrapper">
                <div className="row header no-margin box-shadow">
                    <div className="col-lg-12 top_head">
                        <div className="col-lg-10 right_side">
                            <div className="input-group">
                                <span className="input-group-btn">
                                    <span className="searchimage icon-search"></span>
                                </span>
                                <span className="icon-switch"><input type="text" placeholder="Search by Employee ID" /></span>
                                <span className="input-group-btn">
                                    <input name="btnsearch" type="button" value="Search" className="s-btn-default searchheader" />
                                </span>
                            </div>
                        </div>
                        <div className="pro-comp">
                            <div className="user_details pointer_cursor">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td className="hello">Hello</td>
                                        </tr>
                                        <tr>
                                            <td className="user_name">Admin </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div className="logout text-center pointer_cursor">
                                LOGOUT
                                <span className="icon-logout"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <Sidebar />
                <div className="page-content-wrapper bg-white">
                    <div className="row employee-1">
                        <div className="col-sm-2 col-md-2">
                            <div className="stepwizard">
                                <div className="stepwizard-row setup-panel">
                                    <div className="stepwizard-step active">
                                        <a href="#step-1" className="btn-circle"></a>
                                        <p>Step 1</p>
                                    </div>
                                    <div className="stepwizard-step">
                                        <a href="#step-2" className="btn-circle"></a>
                                        <p>Step 2</p>
                                    </div>
                                    <div className="stepwizard-step">
                                        <a href="#step-3" className="btn-circle"></a>
                                        <p>Step 3</p>
                                    </div>
                                    <div className="stepwizard-step">
                                        <a href="#step-3" className="btn-circle"></a>
                                        <p>Step 4</p>
                                    </div>
                                    <div className="stepwizard-step">
                                        <a href="#step-3" className="btn-circle"></a>
                                        <p>Step 5</p>
                                    </div>
                                    <div className="stepwizard-step">
                                        <a href="#step-3" className="btn-circle"></a>
                                        <p>Step 6</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-sm-10 col-md-10 filters input-space">
                            <div className="row">
                                <div className="col-sm-6 col-md-6">
                                    <input type="text" name="txtEmployeeName" ref="fldName" onChange={this.setEmployeeName} value={this.state.employeeName} required />
                                    <span className="floating-label">Name</span>
                                    {this.state.nameError &&
                                        <div className="error-txt">
                                            {this.state.nameError}
                                        </div>}
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-6 col-md-6">
                                    <h3>Residential Details</h3>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-6 col-md-6">
                                    <input type="text" name="txtStreetAddress" ref="fldStreetAddress" onChange={this.setStreetAddress} value={this.state.streetAddress} required />
                                    <span className="floating-label">Street Address</span>
                                    {this.state.saError &&
                                        <div className="error-txt">
                                            {this.state.saError}
                                        </div>}
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-6 col-md-6">
                                    <div className="filtericon">
                                        <select name="slState" ref="fldState" onChange={this.selectState} value={this.state.slState}>
                                            {this.props.states && map(this.props.states, (val, i) => {
                                                return (
                                                    <option key={val.id} value={val.id}>{val.name}</option>
                                                );
                                            })}
                                        </select>
                                    </div>
                                    {this.state.stateError &&
                                        <div className="error-txt">{this.state.stateError}</div>
                                    }
                                </div>
                                <div className="col-sm-6 col-md-6">
                                    <div className="filtericon">
                                        <select name="slCity" ref="fldCity" onChange={this.selectCity} value={this.state.slCity}>
                                            {this.props.cities && map(this.props.cities, (val, i) => {
                                                return (
                                                    val.state_id === Number(this.state.slState) &&
                                                    <option key={val.id} value={val.id}>{val.name}</option>
                                                );
                                            })}
                                        </select>
                                    </div>
                                    {this.state.cityError &&
                                        <div className="error-txt">
                                            {this.state.cityError}
                                        </div>}
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-6 col-md-6">
                                    <input type="text" name="txtPincode" ref="fldPincode" onChange={this.setPincode} value={this.state.pincode} required />
                                    <span className="floating-label">Pincode</span>
                                    {this.state.pincodeError &&
                                        <div className="error-txt">{this.state.pincodeError}</div>
                                    }
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-12 col-md-12 c-box-custom">
                                    <h3>PERMANENT ADDRESS DETAILS</h3>
                                    <div className="check-mark">
                                        <input type="checkbox" ref="fldSP" onClick={this.isSame} checked={this.state.isSame ? true : false} />
                                        <span className="icon-check-mark"></span>
                                    </div>
                                    Same as Residential
                         </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-6 col-md-6">
                                    <input type="text" name="txtStreetAddress" ref="fldPStreetAddress" onChange={this.setPStreetAddress} value={this.state.pStreetAddress} required />
                                    <span className="floating-label">Street Address</span>
                                    {this.state.pSaError &&
                                        <div className="error-txt">{this.state.pSaError}</div>
                                    }
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-6 col-md-6">
                                    <div className="filtericon">
                                        <select name="slPState" ref="fldPState" onChange={this.selectPState} value={Number(this.state.slPState)}>
                                            {this.props.states && map(this.props.states, (val, i) => {
                                                return (
                                                    <option key={val.id} value={val.id}>{val.name}</option>
                                                );
                                            })}
                                        </select>
                                    </div>
                                    {this.state.pStateError &&
                                        <div className="error-txt">{this.state.pStateError}</div>
                                    }
                                </div>
                                <div className="col-sm-6 col-md-6">
                                    <div className="filtericon">
                                        <select name="slPCity" ref="fldPCity" onChange={this.selectPCity} value={Number(this.state.slPCity)}>
                                            {this.props.cities && map(this.props.cities, (val, i) => {
                                                return (
                                                    val.state_id === Number(this.state.slPState) &&
                                                    <option key={val.id} value={val.id}>{val.name}</option>
                                                );
                                            })}
                                        </select>
                                    </div>
                                    {this.state.pCityError &&
                                        <div className="error-txt">{this.state.pCityError}</div>
                                    }
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-6 col-md-6">
                                    <input type="text" name="txtPincode" ref="fldPPincode" onChange={this.setPPincode} value={this.state.pPincode} required />
                                    <span className="floating-label">Pincode</span>
                                    {this.state.pPincodeError &&
                                        <div className="error-txt">{this.state.pPincodeError}</div>
                                    }
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-6 col-md-6">
                                    <input type="submit" value="Next" onClick={this.loadNext} className="s-btn-primary custom"
                                        name="sbtnNext" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
        states: state.Employees.states,
        cities: state.Employees.cities,
        currentStep: state.Employees.currentStep,
        empMasterData: state.Employees.empMasterData,
        currentEmp: state.Employees.currEmployee,
        userDetails: state.User.userDetails
    };
};

let mapDispatchToProps = (dispatch) => {
    return {
        dispatch: dispatch
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(AddEmployeeS1));
