import React from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router';
import { bindActionCreators } from 'redux';
import * as EmployeeActions from './../../../actions/employee_actions';
import './../../../sass/employee.scss';
import './../../../sass/variables-s.scss';
import Sidebar from './../../../components/sidebar.jsx';
import Modal from 'react-modal';
let AddEmployeeS3 = React.createClass({
    loadPrevious() {
        this.props.dispatch(EmployeeActions.employeeLoadNext(2));
    },
    isEmpty(value) {
        if (value.trim() === "") {
            return true;
        }
        return false;
    },
    loadNext() {
        var bankName = this.refs.fldBankName.value;
        var ifscCode = this.refs.fldIfscCode.value;
        var accNo = this.refs.fldAccNo.value;
        var accType = this.refs.fldAccType.value;
        this.setState({
            bnError: false,
            icError: false,
            anError: false,
            atError: false,
        });
        let error = false;
        if (this.isEmpty(bankName)) {
            error = true;
            this.setState({
                bnError: "Bank Name cannot be left blank."
            });
        }
        if (this.isEmpty(ifscCode)) {
            error = true;
            this.setState({
                icError: "IFSC Code cannot be left blank."
            });
        }
        if (this.isEmpty(accNo)) {
            error = true;
            this.setState({
                anError: "Account Number cannot be left blank."
            });
        }
        if (!error) {
            let payload = {
                id: this.props.currentEmp.id, bank_name: this.state.bankName, bank_ifsc_code: this.state.ifscCode, acc_no: this.state.accNo, acc_type: this.state.accType
            };
            this.props.dispatch(EmployeeActions.crudEmployeeBank(payload));
        }
    },
    setBankName(event) {
        this.setState({
            bankName: event.target.value
        });
        if (event.target.value.trim() != "") {
            this.state.bnError = false
        } else {
            this.state.bnError = "Bank Name cannot be left blank."
        }
    },
    setIfscCode(event) {
        this.setState({
            ifscCode: event.target.value
        });
        if (event.target.value.trim() != "") {
            this.state.icError = false
        } else {
            this.state.icError = "IFSC Code cannot be left blank."
        }
    },
    setAccNo(event) {
        this.setState({
            accNo: event.target.value
        });
        if (event.target.value.trim() != "") {
            this.state.anError = false
        } else {
            this.state.anError = "Account Number cannot be left blank."
        }
    },
    setAccType(event) {
        this.setState({
            accType: event.target.value
        });
    },
    componentWillReceiveProps(nextProps) {
        if (nextProps.empBankData.data && nextProps.empBankData.data.status_code === 200) {
            this.props.dispatch(EmployeeActions.employeeLoadNext(4));
        }
    },
    componentWillMount() {
        this.setState({
            bankName: "",
            ifsCode: "",
            accNo: "",
            accType: 0,
        })
        if (this.props.currentEmp && this.props.currentEmp.bank) {
            this.setState({
                bankName: this.props.currentEmp.bank.bank_name,
                ifscCode: this.props.currentEmp.bank.bank_ifsc_code,
                accNo: this.props.currentEmp.bank.acc_no,
                accType: this.props.currentEmp.bank.acc_type,
            });
        }
    },
    render() {
        return (
            <div className="wrapper">
                <div className="row header no-margin box-shadow">
                    <div className="col-lg-12 top_head">
                        <div className="col-lg-10 right_side">
                            <div className="input-group">
                                <span className="input-group-btn">
                                    <span className="searchimage icon-search"></span>
                                </span>
                                <span className="icon-switch"><input type="text" placeholder="Search by Employee ID" /></span>
                                <span className="input-group-btn">
                                    <input name="btnsearch" type="button" value="Search" className="s-btn-default searchheader" />
                                </span>
                            </div>
                        </div>
                        <div className="pro-comp">
                            <div className="user_details pointer_cursor">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td className="hello">Hello</td>
                                        </tr>
                                        <tr>
                                            <td className="user_name">Admin </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div className="logout text-center pointer_cursor">
                                LOGOUT
                                <span className="icon-logout"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <Sidebar />
                <div className="page-content-wrapper bg-white">
                    <div className="row employee-1">
                        <div className="col-sm-2 col-md-2">
                            <div className="stepwizard">
                                <div className="stepwizard-row setup-panel">
                                    <div className="stepwizard-step active">
                                        <a href="#step-1" className="btn-circle"></a>
                                        <p>Step 1</p>
                                    </div>
                                    <div className="stepwizard-step active">
                                        <a href="#step-2" className="btn-circle"></a>
                                        <p>Step 2</p>
                                    </div>
                                    <div className="stepwizard-step active">
                                        <a href="#step-3" className="btn-circle"></a>
                                        <p>Step 3</p>
                                    </div>
                                    <div className="stepwizard-step">
                                        <a href="#step-3" className="btn-circle"></a>
                                        <p>Step 4</p>
                                    </div>
                                    <div className="stepwizard-step">
                                        <a href="#step-3" className="btn-circle"></a>
                                        <p>Step 5</p>
                                    </div>
                                    <div className="stepwizard-step">
                                        <a href="#step-3" className="btn-circle"></a>
                                        <p>Step 6</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-sm-10 col-md-10 filters input-space">
                            <div className="row">
                                <h3>Bank Details</h3>
                            </div>
                            <div className="row">
                                <div className="col-sm-6 col-md-6">
                                    <input type="text" name="txtBankName" ref="fldBankName" onChange={this.setBankName} value={this.state.bankName} required />
                                    <span className="floating-label">Bank Name</span>
                                    {this.state.bnError &&
                                        <div className="error-txt">
                                            {this.state.bnError}
                                        </div>}
                                </div>
                                <div className="col-sm-6 col-md-6">
                                    <input type="text" name="txtIfscCode" ref="fldIfscCode" onChange={this.setIfscCode} value={this.state.ifscCode} required />
                                    <span className="floating-label">Bank IFSC Code</span>
                                    {this.state.icError &&
                                        <div className="error-txt">{this.state.icError}</div>}
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-6 col-md-6">
                                    <input type="text" name="txtAccNo" ref="fldAccNo" onChange={this.setAccNo} value={this.state.accNo} required />
                                    <span className="floating-label">Account No</span>
                                    {this.state.anError &&
                                        <div className="error-txt">
                                            {this.state.anError}
                                        </div>}
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-2 col-md-2">
                                    Account Type
                                    </div>
                                <div className="col-sm-2 col-md-2">
                                    <input type="radio" name="rbAccType" ref="fldAccType" value="0" checked={this.state.accType == "0" ? true : false}
                                        onClick={this.setAccType} /><label className="radiolabel">Savings</label>
                                </div>
                                <div className="col-sm-2 col-md-2">
                                    <input type="radio" name="rbAccType" ref="fldAccType" value="1" checked={this.state.accType == "1" ? true : false}
                                        onClick={this.setAccType} /> <label className="radiolabel">Commercial</label>
                                </div>
                            </div>
                            <div className="row siteDetails">
                                <div className="col-sm-6 col-md-6">
                                    <h3>Site Details</h3>
                                </div>
                                <div className="col-sm-6 col-md-6 text-right">
                                    <input type="button" name="btnsearch" value="Add Designation" className="s-btn-addemployee custom" />
                                </div>
                            </div>
                            <div className="content-table">
                                <table className="table table-striped table-fixed">
                                    <thead>
                                        <th>Sitename</th>
                                        <th>Site Code</th>
                                        <th>Designation</th>
                                        <th>Location</th>
                                        <th>Actions</th>
                                    </thead>
                                    <tr>
                                        <td>Site 1</td>
                                        <td>Site 003</td>
                                        <td>Designation 1</td>
                                        <td>Pune</td>
                                        <td>
                                            <div className="action_list">
                                                <a href="#">
                                                    <label className='toggler__label'>
                                                        <input type="checkbox" hidden />
                                                        <div className='toggler'></div>
                                                    </label>
                                                </a>
                                                <a href="#"><span className="icon-edit"></span></a>
                                                <a href="#"><span className="icon-delete"></span></a>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <div className="row">
                                    <div className="col-sm-6 col-md-6">
                                        <input type="submit" value="Previous" onClick={this.loadPrevious} className="s-btn-primary custom previous_btn "
                                            name="sbtnBack" />
                                        <input type="submit" value="Next" onClick={this.loadNext} className="s-btn-primary custom"
                                            name="sbtnNext" />
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <Modal isOpen={false} contentLabel="Modal" className="addDesignation">
                    <div className="cover">
                        <div className="content">
                            <div className="row">
                                <div className="col-sm-12 col-md-12 filtericon">
                                    <select name="slSiteCode">
                                        <option>Site Code</option>
                                    </select>
                                </div>
                                <div className="col-sm-12 col-md-12 filtericon">
                                    <select name="slDesignation">
                                        <option>Designation</option>
                                    </select>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <input type="submit" value="Save" onClick={this.loadPrevious} className="s-btn-primary custom previous_btn "
                                    name="sbtnBack" />
                                <input type="submit" value="Cancel" onClick={this.loadNext} className="s-btn-primary custom"
                                    name="sbtnNext" />
                            </div>
                        </div>
                    </div>
                </Modal>

            </div>

        );
    }
});


let mapStateToProps = (state) => {

    return {
        currentStep: state.Employees.currentStep,
        currentEmp: state.Employees.currEmployee,
        empBankData: state.Employees.empBankData,
        userDetails: state.User.userDetails,
        maritalStatuses: state.Employees.maritalStatuses,
        bloodGroups: state.Employees.bloodGroups
    };
};

let mapDispatchToProps = (dispatch) => {
    return {
        dispatch: dispatch
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(AddEmployeeS3));

