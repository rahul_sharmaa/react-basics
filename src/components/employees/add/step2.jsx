import React from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router';
import { bindActionCreators } from 'redux';
import * as EmployeeActions from './../../../actions/employee_actions';
import './../../../sass/employee.scss';
import './../../../sass/variables-s.scss';
import Sidebar from './../../../components/sidebar.jsx';
import map from 'lodash/map';
import DatePicker from 'react-datepicker';
import moment from 'moment';

import 'react-datepicker/dist/react-datepicker.css';

let AddEmployeeS2 = React.createClass({
    loadPrevious() {
        this.props.dispatch(EmployeeActions.employeeLoadNext(1));
    },
    isEmpty(value) {
        if (value.trim() === "") {
            return true;
        }
        return false;
    },
    loadNext() {
        var dob = this.refs.fldDob.value;
        var fname = this.refs.fldFName.value;
        var sname = this.refs.fldSName.value;
        var gender = this.refs.fldGender.value;
        var bloodGroup = this.refs.fldBloodGroup.value;
        var maritalStatus = this.refs.fldMaritalStatus.value;
        var children = this.refs.fldChildren.value;
        var eduQual = this.refs.fldEduQual.value;
        var mobile = this.refs.fldMobile.value;
        var contact = this.refs.fldContact.value;

        this.setState({
            dobError: false,
            fNameError: false,
            sNameError: false,
            genderError: false,
            bgError: false,
            msError: false,
            childrenError: false,
            eqError: false,
            mError: false,
            cError: false,
        })
        let error = false;
        if (this.isEmpty(this.state.dob)) {
            error = true;
            this.setState({
                dobError: "DOB cannot be left blank."
            });
        }
        if (this.isEmpty(this.state.fName)) {
            error = true;
            this.setState({
                fNameError: "Father's name cannot be left blank."
            });
        }
        if (this.isEmpty(this.state.eduQual)) {
            error = true;
            this.setState({
                eqError: "Educational Qualification cannot be left blank."
            });
        }
        if (this.isEmpty(this.state.mobile)) {
            error = true;
            this.setState({
                mError: "Mobile number cannot be left blank."
            });
        }
        if (this.isEmpty(this.state.contact)) {
            error = true;
            this.setState({
                cError: "Alternate contact number cannot be left blank."
            });
        }
        if (!error) {
            let payload = {
                id: this.props.currentEmp.id, dob: this.state.dob, father_name: this.state.fName, spouse_name: this.state.sName, gender: this.state.gender,
                blood_group: this.state.bloodGroup, marital_status: this.state.maritalStatus, no_of_children: this.state.children, edu_qualification: this.state.eduQual, mobile_no: this.state.mobile,
                alt_contact_no: this.state.contact
            };
            this.props.dispatch(EmployeeActions.crudEmployeePersonal(payload));
        }
    },
    componentWillReceiveProps(nextProps) {
        if (nextProps.empPersonalData.data && nextProps.empPersonalData.data.status_code === 200) {
            this.props.dispatch(EmployeeActions.employeeLoadNext(3));
        }
    },
    componentWillMount() {
        this.setState({
            dob: '1990-10-10',
            fName: "",
            sName: "",
            gender: "m",
            bloodGroup: 0,
            maritalStatus: 0,
            children: 0,
            eduQual: "",
            mobile: "",
            contact: "",
        })

        if (this.props.currentEmp && this.props.currentEmp.personal) {
            this.setState({
                dob: this.props.currentEmp.personal.dob,
                fName: this.props.currentEmp.personal.father_name,
                sName: this.props.currentEmp.personal.spouse_name,
                gender: this.props.currentEmp.personal.gender,
                bloodGroup: this.props.currentEmp.personal.blood_group,
                maritalStatus: this.props.currentEmp.personal.marital_status,
                children: this.props.currentEmp.personal.no_of_children,
                eduQual: this.props.currentEmp.personal.edu_qualification,
                mobile: this.props.currentEmp.personal.mobile_no,
                contact: this.props.currentEmp.personal.alt_contact_no,
            });
        }
    },
    setDob(event) {
        this.setState({
            dob: event.target.value
        });
        if (event.target.value.trim() != "") {
            this.state.dobError = false
        } else {
            this.state.dobError = "DOB cannot be left blank."
        }
    },
    handleChange(date) {
        this.setState({
          dob: date
        });
    },
    setFName(event) {
        this.setState({
            fName: event.target.value
        });
        if (event.target.value.trim() != "") {
            this.state.fNameError = false
        } else {
            this.state.fNameError = "Fathers Name cannot be left blank."
        }
    },
    setSName(event) {
        this.setState({
            sName: event.target.value
        });
    },
    setChildren(event) {
        this.setState({
            children: event.target.value
        });
    },
    setMaritalStatus(event) {
        this.setState({
            maritalStatus: event.target.value
        });
    },
    setBloodGroup(event) {
        this.setState({
            bloodGroup: event.target.value
        });
    },
    setGender(event) {
        this.setState({
            gender: event.target.value
        });
    },
    setEduQual(event) {
        this.setState({
            eduQual: event.target.value
        });
        if (event.target.value.trim() != "") {
            this.state.eqError = false
        } else {
            this.state.eqError = "Educational Qualification cannot be left blank."
        }
    },
    setMobile(event) {
        this.setState({
            mobile: event.target.value
        });
        if (event.target.value.trim() != "") {
            this.state.mError = false
        } else {
            this.state.mError = "Mobile number cannot be left blank."
        }
    },
    setContact(event) {
        this.setState({
            contact: event.target.value
        });
        if (event.target.value.trim() != "") {
            this.state.cError = false
        } else {
            this.state.cError = "Alternate Contact number cannot be left blank."
        }
    },
    render() {
        var _this = this;
        return (
            <div className="wrapper">
                <div className="row header no-margin box-shadow">
                    <div className="col-lg-12 top_head">
                        <div className="col-lg-10 right_side">
                            <div className="input-group">
                                <span className="input-group-btn">
                                    <span className="searchimage icon-search"></span>
                                </span>
                                <span className="icon-switch"><input type="text" placeholder="Search by Employee ID" /></span>
                                <span className="input-group-btn">
                                    <input name="btnsearch" type="button" value="Search" className="s-btn-default searchheader" />
                                </span>
                            </div>
                        </div>
                        <div className="pro-comp">
                            <div className="user_details pointer_cursor">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td className="hello">Hello</td>
                                        </tr>
                                        <tr>
                                            <td className="user_name">Admin </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div className="logout text-center pointer_cursor">
                                LOGOUT
                                        <span className="icon-logout"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <Sidebar />
                <div className="page-content-wrapper bg-white">
                    <div className="row employee-1">
                        <div className="col-sm-2 col-md-2">
                            <div className="stepwizard">
                                <div className="stepwizard-row setup-panel">
                                    <div className="stepwizard-step active">
                                        <a href="#step-1" className="btn-circle"></a>
                                        <p>Step 1</p>
                                    </div>
                                    <div className="stepwizard-step active">
                                        <a href="#step-2" className="btn-circle"></a>
                                        <p>Step 2</p>
                                    </div>
                                    <div className="stepwizard-step">
                                        <a href="#step-3" className="btn-circle"></a>
                                        <p>Step 3</p>
                                    </div>
                                    <div className="stepwizard-step">
                                        <a href="#step-3" className="btn-circle"></a>
                                        <p>Step 4</p>
                                    </div>
                                    <div className="stepwizard-step">
                                        <a href="#step-3" className="btn-circle"></a>
                                        <p>Step 5</p>
                                    </div>
                                    <div className="stepwizard-step">
                                        <a href="#step-3" className="btn-circle"></a>
                                        <p>Step 6</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-sm-10 col-md-10 filters input-space">
                            <div className="row">
                                <div className="col-sm-6 col-md-6">
                                    <input type="text" name="txtDob" ref="fldDob" onChange={this.setDob} value={this.state.dob} required />
                                    <span className="floating-label">Date Of birth</span>
                                    {this.state.dobError &&
                                        <div className="error-txt">
                                            {this.state.dobError}
                                        </div>}
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-6 col-md-6">
                                    <input type="text" name="txtFathersName" ref="fldFName" onChange={this.setFName} value={this.state.fName} required />
                                    <span className="floating-label">Father's Name</span>
                                    {this.state.fNameError &&
                                        <div className="error-txt">
                                            {this.state.fNameError}
                                        </div>}
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-6 col-md-6 gender_radio">
                                    <span>Gender</span>
                                    <div className="radiostep4margin">
                                        <input type="radio" name="rbGender" ref="fldGender" value="m" onClick={this.setGender} checked={this.state.gender === "m" ? true : false} className="radiocustom" />
                                        <label className="radiolabel">Male</label>
                                        <input type="radio" name="rbGender" ref="fldGender" value="f" onClick={this.setGender} checked={this.state.gender === "f" ? true : false} className="radiocustom" /><label className="radiolabel">Female</label>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-6 col-md-6 filtericon">
                                    <select name="slBloodGroup" ref="fldBloodGroup" onChange={this.setBloodGroup} value={this.state.bloodGroup}>
                                        {this.props.bloodGroups && map(this.props.bloodGroups, (val, i) => {
                                            return (
                                                <option value={val.id}>{val.name}</option>
                                            );
                                        })}
                                    </select>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-6 col-md-6 filtericon">
                                    <select name="slMaritalStatus" ref="fldMaritalStatus" onChange={this.setMaritalStatus} value={this.state.maritalStatus}>
                                        {this.props.maritalStatuses && map(this.props.maritalStatuses, (val, i) => {
                                            return (
                                                <option value={val.id}>{val.name}</option>
                                            );
                                        })}
                                    </select>
                                </div>
                                <div className="col-sm-6 col-md-6">
                                    <input type="text" name="txtSpouseName" ref="fldSName" onChange={this.setSName} value={this.state.sName} className="inputbtn_margin" required />
                                    <span className="floating-label">Spouse Name</span>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-6 col-md-6 filtericon">
                                    <input type="text" name="txtNoOfChildren" ref="fldChildren" value={this.state.children} onChange={this.setChildren} required />
                                    <span className="floating-label">No Of Children</span>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-6 col-md-6">
                                    <input type="text" name="txtEduQualification" ref="fldEduQual" onChange={this.setEduQual} value={this.state.eduQual} required />
                                    <span className="floating-label">Educational Qualification</span>
                                    {this.state.eqError &&
                                        <div className="error-txt">
                                            {this.state.eqError}
                                        </div>}
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-6 col-md-6">
                                    <input type="text" name="txtMobile" ref="fldMobile" onChange={this.setMobile} value={this.state.mobile} required />
                                    <span className="floating-label">Mobile Number</span>
                                    {this.state.mError &&
                                        <div className="error-txt">
                                            {this.state.mError}
                                        </div>}
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-6 col-md-6">
                                    <input type="text" name="txtAltContact" ref="fldContact" onChange={this.setContact} value={this.state.contact} required />
                                    <span className="floating-label">Alternative Contact Number</span>
                                    {this.state.cError &&
                                        <div className="error-txt">
                                            {this.state.cError}
                                        </div>}
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-6 col-md-6">
                                    <input type="submit" value="Previous" onClick={this.loadPrevious} className="s-btn-primary custom previous_btn "
                                        name="sbtnBack" />
                                    <input type="submit" value="Next" onClick={this.loadNext} className="s-btn-primary custom"
                                        name="sbtnNext" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
        currentStep: state.Employees.currentStep,
        currentEmp: state.Employees.currEmployee,
        empPersonalData: state.Employees.empPersonalData,
        userDetails: state.User.userDetails,
        maritalStatuses: state.Employees.maritalStatuses,
        bloodGroups: state.Employees.bloodGroups
    };
};

let mapDispatchToProps = (dispatch) => {
    return {
        dispatch: dispatch
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(AddEmployeeS2));

