import React from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router';
import { bindActionCreators } from 'redux';
import * as EmployeeActions from './../../actions/employee_actions';
import * as UserActions from './../../actions/user_actions';
import Sidebar from './../../components/sidebar.jsx';
import map from 'lodash/map';
import './../../sass/employee.scss';
import './../../sass/variables-s.scss';
import './../../sass/bootstrap/_pagination.scss';
import './../../sass/bootstrap/_tables.scss';
import Pagination from "react-js-pagination";

let ListEmployees = React.createClass({
    componentWillMount() {
        let totalLength = 0;
        let perPage = 10;
        if (this.props.employees && this.props.employees) {
            totalLength = Object.keys(this.props.employees).length;
        }
        this.setState({
            activePage: 1,
            start: 0,
            perPage: 8,
            totalDataCount: totalLength,
            data: []
        })
        this.refreshData(0, 8);
    },
    componentWillReceiveProps(nextProps) {
        if(!nextProps.userDetails.data) {
            this.props.router.push('/');
        }
        if (nextProps.currentEmp && Number(nextProps.currentEmp.id) > 0 && nextProps.currentEmp.done) {
            
        }
        if (nextProps.currentEmp && Number(nextProps.currentEmp.id) > 0 && !nextProps.currentEmp.done) {
            this.props.router.push('/employees/add');
        }
    },
    handlePageChange(pageNumber) {
        this.setState({
            activePage: pageNumber
        });
        let start = (pageNumber * this.state.perPage) - this.state.perPage;
        let perPage = start + this.state.perPage;
        this.setState({
            start: start,
        })
        this.refreshData(start, perPage)
    },
    handleKeyPress(event) {
        if (event.key == "Enter") {
            this.searchData()
        }
    },
    searchData() {
        let data = [];
        let term = this.refs.fldSearchTerm.value;
        if (term.trim() && this.props.employees && this.props.employees) {
            let count = 0;
            map(this.props.employees, (val, i) => {
                if (val.employee_code === term.toUpperCase()) {
                    data.push(val);
                    ++count;
                }
            })
            this.setState({
                data: data,
                totalDataCount: count
            })
        } else {
            if (this.props.employees) {
                this.setState({
                    totalDataCount: Object.keys(this.props.employees).length,
                    start: 0,
                })
            }
            this.refreshData(this.state.start, this.state.perPage);
        }
    },
    refreshData(start = 0, perPage = 8) {
        let data = [];
        if (this.props.employees)
            data = this.props.employees.slice(start, perPage);

        this.setState({
            data: data
        })
    },
    addEmployee() {
        this.props.dispatch(EmployeeActions.refreshAddEmployee());
        this.props.router.push('/employees/add');
    },
    getEmployee(event) {
        this.props.dispatch(EmployeeActions.getEmployeeDetail(event.target.id));
    },
    deleteEmployee(event) {
        this.props.dispatch(EmployeeActions.deleteEmployee({id: event.target.id}));
        window.setTimeout(() => {this.props.router.push('/')}, 1000);
    },
    setStatus(event){
        console.log(event.target.id);
    },
    logout(){
        this.props.dispatch(UserActions.logout());
        window.setTimeout(() => {this.props.router.push('/')}, 10);
    },
    render() {
        return (
            <div className="wrapper">
                {this.props.dataTime &&
                    <div>
                        {console.log(this.props.dataTime)}
                    </div>
                }
                <div className="row header no-margin box-shadow">
                    <div className="col-lg-12 top_head">
                        <div className="col-lg-10 right_side">
                            <div className="input-group">
                                <span className="input-group-btn">
                                    <span className="searchimage icon-search"></span>
                                </span>
                                <span className="icon-switch"><input type="text" ref="fldSearchTerm" onKeyPress={this.handleKeyPress} placeholder="Search by Employee ID" /></span>
                                <span className="input-group-btn">
                                    <input name="btnsearch" type="button" onClick={this.searchData} value="Search" className="s-btn-default searchheader" />
                                </span>
                            </div>
                        </div>
                        <div className="pro-comp">
                            <div className="user_details pointer_cursor">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td className="hello">Hello</td>
                                        </tr>
                                        <tr>
                                            <td className="user_name">Admin </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div className="logout text-center pointer_cursor" onClick={this.logout}>
                                LOGOUT
                                        <span className="icon-logout"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <Sidebar />
                <div className="page-content-wrapper">
                    <div className="page-content">
                        <div className="row filters">
                            <div className="col-md-2"><div className="filtericon selectafter"><select name="slZone" className="form-control selectwidth"><option>Zone</option></select></div></div>
                            <div className="col-md-2"><div className="filtericon selectafter"><select name="slSector" className="form-control selectwidth"><option>Sector</option></select></div></div>
                            <div className="col-md-2"><div className="filtericon selectafter"><select name="slGroup" className="form-control selectwidth"><option>Group</option></select></div></div>
                            <div className="col-md-2"><div className="filtericon selectafter"><select name="slBMD" className="form-control selectwidth"><option>BDM</option></select></div></div>
                            <div className="col-md-4 text-right">
                                <input name="btnsearch" type="button" value="Add employee" onClick={this.addEmployee} className="s-btn-addemployee custom" />
                            </div>
                        </div>
                        <div className="container-fluid content-table">
                            <table className="table table-striped table-fixed">
                                <thead>
                                    <tr>
                                        <th>S.NO.</th>
                                        <th>EMP NAME</th>
                                        <th>CODE</th>
                                        <th>DATE OF BIRTH</th>
                                        <th>CONTACT</th>
                                        <th>ACTIONS</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state && this.state.data &&
                                        map(this.state.data, (val, ind) => {
                                            return (
                                                <tr key={'employee' + ind}>
                                                    <td>{val.id}</td>
                                                    <td>{val.employee_name}</td>
                                                    <td>{val.employee_code}</td>
                                                    <td>{val.dob}</td>
                                                    <td>{val.mobile_no}</td>
                                                    <td>
                                                        <div className="action_list">
                                                            <a href="#">
                                                                <label className='toggler__label'>
                                                                    <input type="checkbox" id={val.id} onClick={this.setStatus} hidden />
                                                                    <div className='toggler'></div>
                                                                </label>
                                                            </a>
                                                            <a href="#" onClick={this.getEmployee}><span id={val.id} className="icon-edit"></span></a>
                                                            <a href="#" onClick={this.deleteEmployee}><span id={val.id} className="icon-delete"></span></a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            );
                                        })
                                    }
                                </tbody>
                            </table>
                            <Pagination
                                activePage={this.state.activePage}
                                itemsCountPerPage={this.state.perPage}
                                totalItemsCount={this.state.totalDataCount}
                                pageRangeDisplayed={5}
                                onChange={this.handlePageChange} />
                        </div>
                    </div>
                </div>
            </div>

        );
    }
});

let mapStateToProps = (state) => {
    return {
        currentEmp: state.Employees.currEmployee,
        employees: state.Employees.listEmployees,
        employeesL: state.Employees.listEmployees.length,
        userDetails: state.User.userDetails
    };
};

let mapDispatchToProps = (dispatch) => {
    return {
        dispatch: dispatch
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ListEmployees));
