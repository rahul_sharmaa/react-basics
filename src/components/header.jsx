
import React from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router';
import * as screenConfig from './../config/screenConfig';
import { bindActionCreators } from 'redux';
var logo = require('./../images/logo.png');
let Header = React.createClass({

    render() {
        return (
            <div>
                <header>
                    <img src={logo} alt="logo"/>
                </header>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
   
    return { 

    };
};
let mapDispatchToProps = (dispatch) => ({
    dispatch: dispatch
});
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Header));
