import React from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router';
import { bindActionCreators } from 'redux';
import * as EmployeeActions from './../../actions/employee_actions';
import Sidebar from './../../components/sidebar.jsx';
import './../../sass/sites.scss';
import './../../sass/variables-s.scss';
import './../../sass/employee.scss';
import './../../sass/bootstrap/_pagination.scss';
import './../../sass/bootstrap/_tables.scss';
import map from 'lodash/map';

let ListSites = React.createClass({
    componentWillMount() {
    },
    componentWillReceiveProps(nextProps) {
    },
    render() {
        return (
            <div className="wrapper wrappernew">
                <div className="row header no-margin box-shadow">
                    <div className="col-lg-12 top_head">
                        <div className="col-lg-10 right_side">
                            <div className="input-group">
                                <span className="input-group-btn">
                                    <span className="searchimage icon-search"></span>
                                </span>
                                <span className="icon-switch"><input type="text" ref="fldSearchTerm" onKeyPress={this.handleKeyPress} placeholder="Search by Employee ID" /></span>
                                <span className="input-group-btn">
                                    <input name="btnsearch" type="button" onClick={this.searchData} value="Search" className="s-btn-default searchheader" />
                                </span>
                            </div>
                        </div>
                        <div className="pro-comp">
                            <div className="user_details pointer_cursor">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td className="hello">Hello</td>
                                        </tr>
                                        <tr>
                                            <td className="user_name">Admin </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div className="logout text-center pointer_cursor">
                                LOGOUT
                                        <span className="icon-logout"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <Sidebar />
                <div className="page-content-wrapper">
                    <div className="row filters sitesearch_margin">
                        <div className="col-md-2"><div className="filtericon selectafter"><select name="slZone" className="form-control selectwidth"><option>Zone</option></select></div></div>
                        <div className="col-md-2"><div className="filtericon selectafter"><select name="slSector" className="form-control selectwidth"><option>Sector</option></select></div></div>
                        <div className="col-md-2"><div className="filtericon selectafter"><select name="slGroup" className="form-control selectwidth"><option>Group</option></select></div></div>
                        <div className="col-md-2"><div className="filtericon selectafter"><select name="slBMD" className="form-control selectwidth"><option>BMD</option></select></div></div>
                    </div>
                    <div className="page-content">
                        <div className="row">
                            <div className="col-sm-2 col-md-2">
                                <div className="sitedashboard_heading">site data</div>
                            </div>
                            <div className="col-sm-6 col-md-6"></div>
                            <div className="col-sm-4 col-md-4">
                                <input name="btnsearch" type="button" value="Add employee" className="s-btn-addemployee createsitebtn custom" />
                            </div>
                        </div>
                        <div className="container-fluid content-table">
                            <table className="table table-striped table-fixed">
                                <thead>
                                    <tr>
                                        <th>SITE CODE</th>
                                        <th>SITE NAME</th>
                                        <th>SILA POC</th>
                                        <th>SITE POC</th>
                                        <th>CONTRACT VALUE</th>
                                        <th>CONTRACT STATUS</th>
                                        <th>LOCATION</th>
                                        <th>ACTIONS</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>SILA123</td>
                                        <td>SITE 1</td>
                                        <td>POC 1</td>
                                        <td>POC 2</td>
                                        <td>12</td>
                                        <td><span></span></td>
                                        <td>LOC1</td>
                                        <td>
                                            <div className="action_list">
                                                <a href="#">
                                                    <label className='toggler__label'>
                                                        <input type="checkbox" hidden />
                                                        <div className='toggler'></div>
                                                    </label>
                                                </a>
                                                <a href="#"><span className="icon-edit"></span></a>
                                                <a href="#"><span className="icon-delete"></span></a>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
    };
};

let mapDispatchToProps = (dispatch) => {
    return {
        dispatch: dispatch
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ListSites));

