import * as API from './../base/api_service';
import * as serviceCallConfig from './../config/api_urls';
import map from 'lodash/map';
import isArray from 'lodash/isArray';

export const USER_LOGIN = 'USER_LOGIN';
export const USER_LOGIN_SUCCESS = 'USER_LOGIN_SUCCESS';
export const USER_LOGIN_FAILED = 'USER_LOGIN_FAILED';
export const LOGOUT = 'LOGOUT';

export var userLogin = (loginData) => {
    return (dispatch, getState) => {
        dispatch({
            type: USER_LOGIN
        })
        API.makeServiceCall('POST', dispatch, serviceCallConfig.get(serviceCallConfig.USER_LOGIN), 
        loginData, serviceCallConfig.USER_LOGIN, false);

    }
}

export var loginSuccess = (loginData) => {
    return (dispatch, getState) => {
        dispatch({
            type: USER_LOGIN_SUCCESS,
            data:loginData
        })
    }
}

export var loginFailed = (loginData) => {
    return (dispatch, getState) => {
        dispatch({
            type: USER_LOGIN_FAILED,
            data:loginData
        })
    }
}

export var logout = () => {
    return (dispatch, getState) => {
        dispatch({
            type: LOGOUT,
        })
    }
}