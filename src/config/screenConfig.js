export var HEADER_RED_THEME = 'header1';
export var HEADER_WHITE_THEME = 'header2';
export var SCREEN_CONFIG = {
    deiveryPickupScreen: {
        headerTheme: HEADER_RED_THEME,
        hasMenuIcon: false,
        hasSearchField: false,
        hasBackButton: false,
        hasGroupedCartAndMenu: false,
        hasOnlyCart: false,
        backBtnNav: '/'
    },
    otpVerificationScreen: {
        headerTheme: HEADER_RED_THEME,
        hasMenuIcon: false,
        hasBackButton: false,
        hasSearchField: false,
        hasGroupedCartAndMenu: false,
        hasOnlyCart: false,
        backBtnNav: '/'

    },
    fullMenuScreen: {
        headerTheme: HEADER_WHITE_THEME,
        hasLeftIcon: true,
        hasBackButton: false,
        hasMenuIcon: true,
        hasSearchField: true,
        isSearchActive: false,
        hasGroupedCartAndMenu: false,
        hasOnlyCart: true,
        backBtnNav: '/'
    },
    confirmOrder: {
        headerTheme: HEADER_WHITE_THEME,
        hasLeftIcon: true,
        hasBackButton: true,
        hasMenuIcon: true,
        hasSearchField: false,
        hasGroupedCartAndMenu: false,
        hasOnlyCart: false,
        backBtnNav: '/'
    },
    checkOutOrder: {
        headerTheme: HEADER_WHITE_THEME,
        hasMenuIcon: false,
        hasSearchField: false,
        hasBackButton: true,
        hasGroupedCartAndMenu: false,
        hasOnlyCart: false,
        backBtnNav: '/pizzalist'
    },
    defaultConfig: {
        headerTheme: HEADER_WHITE_THEME,
        hasLeftIcon: true,
        hasBackButton: false,
        hasMenuIcon: true,
        hasSearchField: false,
        hasGroupedCartAndMenu: false,
        hasOnlyCart: false,
        backBtnNav: '/'
    },
    forgotPassword: {
        headerTheme: HEADER_RED_THEME,
        hasMenuIcon: false,
        hasBackButton: true,
        hasSearchField: false,
        hasGroupedCartAndMenu: false,
        hasOnlyCart: false,
        backBtnNav: '/login'
    },
    login: {
        headerTheme: HEADER_RED_THEME,
        hasMenuIcon: false,
        hasBackButton: true,
        hasSearchField: false,
        hasGroupedCartAndMenu: false,
        hasOnlyCart: false,
        backBtnNav: '/'

    },
    termsCondition: {
        headerTheme: HEADER_RED_THEME,
        hasMenuIcon: false,
        hasBackButton: true,
        hasSearchField: false,
        hasGroupedCartAndMenu: false,
        hasOnlyCart: false,
        backBtnNav: '/pizzalist'
    },
    addDeliveryAddress: {
        headerTheme: HEADER_WHITE_THEME,
        hasMenuIcon: false,
        hasSearchField: false,
        hasBackButton: true,
        hasGroupedCartAndMenu: false,
        hasOnlyCart: false,
        backBtnNav: '/cart'
    },

    paymentMethods: {
        headerTheme: HEADER_WHITE_THEME,
        hasMenuIcon: false,
        hasSearchField: false,
        hasBackButton: true,
        hasGroupedCartAndMenu: false,
        hasOnlyCart: false,
        backBtnNav: '/cart'
    },
    orderHistory: {
        headerTheme: HEADER_WHITE_THEME,
        hasLeftIcon: true,
        hasBackButton: false,
        hasMenuIcon: true,
        hasSearchField: false,
        isSearchActive: true,
        hasGroupedCartAndMenu: false,
        hasOnlyCart: true,
        backBtnNav: '/'
    },
    registerUser: {
        headerTheme: HEADER_WHITE_THEME,
        hasMenuIcon: false,
        hasBackButton: true,
        hasSearchField: false,
        hasGroupedCartAndMenu: false,
        hasOnlyCart: true,
        backBtnNav: '/'
    },
    guestUserRegistration: {
        headerTheme: HEADER_WHITE_THEME,
        hasMenuIcon: true,
        hasBackButton: false,
        hasSearchField: false,
        hasGroupedCartAndMenu: false,
        hasOnlyCart: true,
        backBtnNav: '/cart'
    },
    socialUser: {
        headerTheme: HEADER_RED_THEME,
        hasMenuIcon: false,
        hasBackButton: true,
        hasSearchField: false,
        hasGroupedCartAndMenu: false,
        hasOnlyCart: false,
        backBtnNav: '/login'
    },
    dealPizzaSelector: {
        headerTheme: HEADER_RED_THEME,
        hasMenuIcon: false,
        hasBackButton: true,
        hasSearchField: false,
        hasGroupedCartAndMenu: false,
        hasOnlyCart: false,
        backBtnNav: '/login'
    },
    DealListing: {
        headerTheme: HEADER_WHITE_THEME,
        hasLeftIcon: true,
        hasBackButton: false,
        hasMenuIcon: true,
        hasSearchField: false,
        hasGroupedCartAndMenu: false,
        hasOnlyCart: true,
        isSearchActive: false,
        backBtnNav: '/dealPizzaSelector'
    },
    dealCheckout: {
        headerTheme: HEADER_WHITE_THEME,
        hasLeftIcon: true,
        hasBackButton: false,
        hasMenuIcon: true,
        hasSearchField: false,
        hasGroupedCartAndMenu: false,
        hasOnlyCart: true,
        isSearchActive: false,
        backBtnNav: '/DealListing'
    }
};
export var getConfigbyScreenName = (screenName) => {
    if (!screenName || !SCREEN_CONFIG[screenName]) {
        return SCREEN_CONFIG.defaultConfig;
    }

    return SCREEN_CONFIG[screenName];
};
