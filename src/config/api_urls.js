export var API_KEY = '';
export var API_KEY_v4 = '';
//export var BASE_URL = 'http://192.168.3.223:82/public/api/';
export var BASE_URL = 'http://localhost/sila/public/api/';
export var BASE_URL_v4 = '';
export var TOKEN = '';
export var GOOGLE_CLIENT_ID = '';
export var FACEBOOK_APP_ID = '';
export var LOCAL_BASE_URL = '';
switch (process.env.NODE_ENV) {
    case 'development':
        break;
    case 'stage':
        break;
    case 'production':
        break;
    default:
       
}

export var USER_LOGIN = 'auth/login';
export var EMP_LIST_ALL = 'employees/all';
export var ADD_EMP_MASTER = 'employees/add';
export var GET_EMP = 'employees/details';
export var DELETE_EMPLOYEE = 'employees/delete';
export var ADD_EMP_PERSONAL = 'employees/details/personal/update';
export var ADD_EMP_BANK = 'employees/details/bank/update';
export var ADD_EMP_WORK = 'employees/details/work/update';
export var ADD_EMP_DOCUMENT = 'employees/details/document/update';
export var ADD_EMP_WAGE = 'employees/taxonomies/fields/save';
export var UPLOAD_ASSET = 'assets/upload';

export var API_GET = 'GET';
export var API_POST = 'POST';
export var get = (url, data) => {
    switch (url) {
        case USER_LOGIN:
            return BASE_URL + USER_LOGIN;
        case EMP_LIST_ALL:
            return BASE_URL + EMP_LIST_ALL;
        case ADD_EMP_MASTER:
            return BASE_URL + ADD_EMP_MASTER;
        case ADD_EMP_PERSONAL:
            return BASE_URL + ADD_EMP_PERSONAL;
        case ADD_EMP_BANK:
            return BASE_URL + ADD_EMP_BANK;
        case ADD_EMP_WORK:
            return BASE_URL + ADD_EMP_WORK;
        case ADD_EMP_DOCUMENT:
            return BASE_URL + ADD_EMP_DOCUMENT;
        case ADD_EMP_WAGE:
            return BASE_URL + ADD_EMP_WAGE;
        case GET_EMP:
            return BASE_URL + GET_EMP;
        case UPLOAD_ASSET:
            return BASE_URL + UPLOAD_ASSET;
        case DELETE_EMPLOYEE:
            return BASE_URL + DELETE_EMPLOYEE;
        default:
            break;
    }
    return 'URL Not configured';
};
