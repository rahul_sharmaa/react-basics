import fetch from 'isomorphic-fetch';
import * as serviceCallConfig from './../config/api_urls';
import { response_Manager } from './api_manager';
import formurlencoded from 'form-urlencoded';

export var makeServiceCall = (reqType, dispatch, endPoint,
    payLoad, requestFrom, isURLEncoded, authKey = '', api_key = '', xhr = false) => {

    //if(xhr) {

    //}

    api_key = api_key.trim() || serviceCallConfig.API_KEY_v4.trim();
    if (!navigator.onLine) {
        return response_Manager(true, dispatch, requestFrom,
            payLoad, {
                message: 'User is offline.'
            }, null);
    }
    let parsedPayload;
    if (!isURLEncoded) {
        parsedPayload = JSON.stringify(payLoad);
    } else {
        parsedPayload = formurlencoded(payLoad);
    }
    authKey = (authKey).trim();
    let custom_headers = new Headers({
        'Content-Type': isURLEncoded ? 'application/x-www-form-urlencoded' : 'application/json',
        'authorization': 'bearer ' + authKey
    });

    let reqObj = {
        headers: custom_headers
    };
    if (!payLoad) {
        reqObj = {
            headers: custom_headers,
            method: reqType,
        };
    } else {
        reqObj = {
            headers: custom_headers,
            method: reqType,
            body: parsedPayload
        };
    }

    return fetch(endPoint, reqObj)
        .then(response => {       
            return response.json();
        }).then(json => {                   
            return response_Manager(false, dispatch, requestFrom,
                payLoad, json, null);
        })
        .catch(err => {
            return response_Manager(true, dispatch, requestFrom,
                payLoad, null, err);
        });
};
