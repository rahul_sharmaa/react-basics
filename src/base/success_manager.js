import * as api_urls from './../config/api_urls';
import * as UserActions from './../actions/user_actions';
import * as EmployeeActions from './../actions/employee_actions';
import { debug } from 'util';


export var success_manager = (dispatch, endpoint, payload, response) => {
    switch (endpoint) {

        case api_urls.USER_LOGIN: {
            return dispatch(UserActions.loginSuccess(response));
        } break;
        case api_urls.EMP_LIST_ALL: {
            return dispatch(EmployeeActions.listAllSuccess(response));
        } break;
        case api_urls.GET_EMP: {
            return dispatch(EmployeeActions.getEmployeeDetailSuccess(response));
        } break;
        case api_urls.ADD_EMP_MASTER: {
            return dispatch(EmployeeActions.crudEmployeeMasterSuccess(response));
        } break;
        case api_urls.ADD_EMP_PERSONAL: {
            return dispatch(EmployeeActions.crudEmployeePersonalSuccess(response));
        } break;
        case api_urls.ADD_EMP_BANK: {
            return dispatch(EmployeeActions.crudEmployeeBankSuccess(response));
        } break;
        case api_urls.ADD_EMP_WORK: {
            return dispatch(EmployeeActions.crudEmployeeWorkSuccess(response));
        } break;
        case api_urls.ADD_EMP_DOCUMENT: {
            return dispatch(EmployeeActions.crudEmployeeDocumentSuccess(response));
        } break;
        case api_urls.ADD_EMP_WAGE: {
            dispatch(EmployeeActions.crudEmployeeWageSuccess(response));
            return refresh_employees(dispatch, payload, response)
        } break;
        case api_urls.UPLOAD_ASSET: {
            return dispatch(EmployeeActions.uploadAssetSuccess(response));
        } break;
        case api_urls.DELETE_EMPLOYEE: {
            dispatch(EmployeeActions.deleteEmployeeSuccess(response));
            return refresh_employees(dispatch, payload, response)
        } break;
        // ------------------------------------------//
        default:
            {
                if (response) {
                    return response;
                } else {
                    return { status: 200, message: 'No data.' };
                }
            }
    }
};

var refresh_employees = (dispatch, payload, response) => {
    return dispatch(EmployeeActions.listAll());
}