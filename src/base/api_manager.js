import {
    success_manager
} from './success_manager';
import {
    failed_manager
} from './failed_manager';

export var response_Manager = (isNetworkError, dispatch,
    endpoint, payload, response, error) => {
    if (!response) {
        return failed_manager(dispatch, endpoint,
            payload, response, error);
    } else if (!isNetworkError && response && Number(response.status_code) === 422) {
        return failed_manager(dispatch, endpoint, payload, response, error);
    } else if (!isNetworkError && response && Number(response.status_code) === 500) {
        return failed_manager(dispatch, endpoint, payload, response, error);
    } else if (!isNetworkError && response && Number(response.status_code) === 200) {
        return success_manager(dispatch, endpoint, payload, response);
    }
    return failed_manager(dispatch, endpoint,
        payload, response, response || error);
};
