/** global event */
import React from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router';
import Landing from './../screens/landingScreen';
import Header from './../components/header.jsx';
import Loader from './../components/loader.jsx';

let Shell = React.createClass({
    getInitialState: function () {
        return {
        };
    },
    render() {
        console.log('shell js render---', new Date())
        return (
            <div>
                {
                    this.props.children || <Landing />
                }
        </div>
        )
    }
});
//
let mapStateToProps = (state) => {
    return {
     
    };
};
let mapDispatchToProps = (dispatch) => ({
    dispatch: dispatch
});
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Shell));