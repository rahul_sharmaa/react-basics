import * as UserActions from './../actions/user_actions';
import extend from 'lodash/extend';

export var User = (state = {
    userDetails: {},
    errorDetails: {}
}, action) => {
    switch (action.type) {
        case 'persist/REHYDRATE': {
            if (action.payload.User) {
                return action.payload.User;
            } else {
                return state;
            }
        } 
        case UserActions.USER_LOGIN: {
            return extend({}, state, { });
        } 
        case UserActions.USER_LOGIN_SUCCESS: {

            return extend({}, state, { userDetails: action.data });
        } 
        case UserActions.LOGOUT: {
            return extend({}, state, { userDetails: {}, errorDetails: {} });
        } 
        default:
            return state;
    }
};