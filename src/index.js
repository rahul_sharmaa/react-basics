import AMPDocument from './base/amp-document';
import './../lib/v0';
import './../lib/amp-sidebar-0.1';
import './../lib/amp-lightbox-0.1';
import './../lib/amp-carousel-0.1';

import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, browserHistory } from 'react-router';
import createExpirationTransform from 'redux-persist-transform-expire';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { persistStore, autoRehydrate } from 'redux-persist';
import asyncLocalStoragefrom from 'redux-persist/storages';
import thunk from 'redux-thunk';
import createLogger from 'redux-logger';

import './css/app.scss';
import AppReducer from './reducers';
import Shell from './base/shell';
import AddEmployee from './screens/employees/addEmployee';
import AddDocument from './screens/employees/adddocument';
import ListEmployee from './screens/employees/listEmployees';
import ListSite from './screens/sites/listSites';
import PageNotFound from './screens/pageNotFound'

let reduxLogger = createLogger();
var startTime = new Date().getTime();
var timeOutTime = startTime + (44 * 60 * 1000);
var previousInterVal;
window.socialFlag = null;
let store = createStore(AppReducer,
    window.devToolsExtension ? window.devToolsExtension() : f => f,
    process.env.NODE_ENV === 'production'
        ? applyMiddleware(thunk)
        : applyMiddleware(thunk, reduxLogger),
    autoRehydrate()

);

let reduxInitTime = new Date();
const expireTransform = createExpirationTransform({
    expireKey: reduxInitTime.setHours(reduxInitTime.getHours() + 2)
})

let persistor = persistStore(store,
    {
        storage: asyncLocalStoragefrom,
        transforms: [expireTransform]
    },
    (err, prevSate) => {
        if (err) {
            console.log(err);
        }
        ReactDOM.render((
            <div>
                <Provider store={store} persistor={persistor}>
                    <Router history={browserHistory}>
                        <Route path='/' component={Shell}>
                            <Route path='/employees/list' component={ListEmployee}></Route>
                            <Route path='/sites/list' component={ListSite}></Route>
                            <Route path='/employees/add' component={AddEmployee}></Route>
                            <Route path='/employees/adddocument' component={AddDocument}></Route>
                            <Route path='*' component={PageNotFound} />
                        </Route>
                    </Router>
                </Provider>
            </div>

        ), document.getElementById('root'));
    });

window.onerror = (message, source, lineno, colno, error) => {
    console.log(message, source, lineno, colno, error);
};

function load(url) {
    return new Promise((resolve, reject) => {
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.async = true;
        script.defer = true;
        script.src = url;
        script.onload = resolve;
        script.onerror = reject;
        document.head.appendChild(script);
    });
}